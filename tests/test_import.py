import cv2
import torch
import pmapcpp
from tqdm import tqdm
import matplotlib.pyplot as plt
import random

pmap = pmapcpp.ChunkMap(100, pmapcpp.MapGen.ONES)
pose = pmapcpp.Pose(torch.tensor([0.5, 0.5]), torch.tensor([0]))

#  pmap.init_chunks(cxmax=3, cxmin=0, cymax=3, cymin=0)
std = 25
shape = (std*2+1, std*2+1)
qr_gauss = torch.zeros(shape)
qr_gauss[std, std] = 1
qr_gauss = torch.tensor(cv2.GaussianBlur(qr_gauss.numpy(), shape, 0))

for i in tqdm(range(100)):
    x = random.randint(0, 100)
    y = random.randint(0, 100)
    pmap.add_grid(x, y, qr_gauss)
#  disp, origin = pmap.gen_map(lcxmin=None, lcxmax=None, lcymin=None, lcymax=None, val_unknown=0)
disp, origin = pmap.stitch_map()
plt.imshow(disp)
plt.show()
