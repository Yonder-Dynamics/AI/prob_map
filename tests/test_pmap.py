import unittest
import itertools
import torch
import pmapcpp
import matplotlib.pyplot as plt
import math

class TestPMap(unittest.TestCase):

    def test_check_path_clear(self):
        pmap = pmapcpp.PMap(res=0.1, chunk_size=100)
        pmap.add_obstacle(50, 50, 1)
        print(pmap.obs_map.get_bounds())
        path1 = [
            pmapcpp.Pose(50, 10, 0),
            pmapcpp.Pose(50, 70, 0),
        ]
        path2 = [
            pmapcpp.Pose(0, 10, 0),
            pmapcpp.Pose(0, 70, 0),
        ]
        assert(not pmap.check_path_clear(path1))
        assert(pmap.check_path_clear(path2))

    def test_add_observation(self):
        pmap = pmapcpp.PMap(res=1, chunk_size=100)
        observation = torch.rand(10, 2)
        observation[:5, 0] = 1
        observation[5:, 0] = 0
        #  observation[:, 1] = observation[:, 1]*5 + 5
        observation[:, 1] = 10
        poses = [
            pmapcpp.Pose(25, 25, math.pi),
            pmapcpp.Pose(20, 30, math.pi/2),
            pmapcpp.Pose(25, 25, math.pi/4)
        ]
        pmap.add_uniform(25, 25, 50)
        im, origin = pmap.prob_map.stitch_map()
        (min_x, min_y), (max_x, max_y) = pmap.prob_map.get_bounds()

        plt.imshow(im, extent=(min_x, max_x, min_y, max_y))
        plt.show()
        for pose in poses:
            pmap.add_observation(pose, observation, 10, 5, 0.1)
        im, origin = pmap.prob_map.stitch_map()
        (min_x, min_y), (max_x, max_y) = pmap.prob_map.get_bounds()

        plt.imshow(im, extent=(min_x, max_x, min_y, max_y))
        plt.show()

    def test_add_uniform(self):
        pmap = pmapcpp.PMap(res=0.1, chunk_size=100)
        def single_test(ox, oy, r):
            pmap.add_uniform(ox, oy, r)
            s = 0
            for x, y in itertools.product(range(ox - r, ox + r + 1), range(oy - r, oy + r + 1)):
                val = pmap.base_tag_prob_map.at(x, y)
                s += val
            print(s)
            self.assertTrue(abs(s - 1) < 0.00001)
        single_test(0, 0, 5)
        single_test(100, 100, 25)






if __name__ == '__main__':
    unittest.main()
