import unittest
import torch
import pmapcpp
import random

class TestGraphClasses(unittest.TestCase):

    # Test Pose constructor
    def test_pose(self):
        pose = pmapcpp.Pose(torch.tensor([0.5, 0.5]), torch.tensor([0]))
        self.assertTrue(torch.all(torch.eq(torch.tensor([0.5, 0.5]), pose.position)))

    # Test length initialization of edge
    def test_edge_length(self):
        pose1 = pmapcpp.Pose(torch.tensor([0.0, 0.0]), torch.tensor([0]))
        pose2 = pmapcpp.Pose(torch.tensor([0.0, 1.0]), torch.tensor([0]))
        pose3 = pmapcpp.Pose(torch.tensor([1.0, 1.0]), torch.tensor([0]))
        path = [pose1, pose2, pose3]
        edge = pmapcpp.Edge(0,0,0.0,path)
        self.assertEqual(2, edge.length)


if __name__ == '__main__':
    unittest.main()