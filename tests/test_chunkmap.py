import unittest
import torch
import pmapcpp

class TestGraphClasses(unittest.TestCase):
    
    # Tests contains_chunk()
    def test_contains_chunk(self):
        pmap = pmapcpp.ChunkMap(100, pmapcpp.MapGen.ZEROS)
        
        #check if (0,0) chunk is generated
        self.assertTrue(pmap.contains_chunk(0, 0))
        self.assertFalse(pmap.contains_chunk(1, 1))

    # tests chunk_set() and chunk_at()
    def test_chunk_set_at(self):
        pmap = pmapcpp.ChunkMap(100, pmapcpp.MapGen.ZEROS)

        #get random chunk locations
        randChunkPos1 = (5, 7)
        randChunkPos2 = (79, 2)
        randChunkPos3 = (-45, -7)
        randChunkPos4 = (8, -77)

        # set empty chunks at above locations
        pmap.chunk_set(randChunkPos1[0], randChunkPos1[1], pmap.gen_empty())
        pmap.chunk_set(randChunkPos2[0], randChunkPos2[1], pmap.gen_empty())
        pmap.chunk_set(randChunkPos3[0], randChunkPos3[1], pmap.gen_empty())
        pmap.chunk_set(randChunkPos4[0], randChunkPos4[1], pmap.gen_empty())

        # check if above chunks added to map
        self.assertTrue(pmap.contains_chunk(randChunkPos1[0], randChunkPos1[1]))
        self.assertTrue(pmap.contains_chunk(randChunkPos2[0], randChunkPos2[1]))
        self.assertTrue(pmap.contains_chunk(randChunkPos3[0], randChunkPos3[1]))
        self.assertTrue(pmap.contains_chunk(randChunkPos4[0], randChunkPos4[1]))

        # Make sure map bounds initialized correctly
        self.assertEqual(-45, pmap.cxmin)
        self.assertEqual(-77, pmap.cymin)
        self.assertEqual(79, pmap.cxmax)
        self.assertEqual(7, pmap.cymax)

        # Test chunk_at()
        self.assertTrue(torch.all(torch.eq(pmap.gen_empty(), pmap.chunk_at(randChunkPos1[0], randChunkPos1[1]))))
        self.assertTrue(torch.all(torch.eq(pmap.gen_empty(), pmap.chunk_at(randChunkPos2[0], randChunkPos2[1]))))
        self.assertTrue(torch.all(torch.eq(pmap.gen_empty(), pmap.chunk_at(randChunkPos3[0], randChunkPos3[1]))))
        self.assertTrue(torch.all(torch.eq(pmap.gen_empty(), pmap.chunk_at(randChunkPos4[0], randChunkPos4[1]))))

    # tests set() and at()
    def test_set_at(self):
        pmap = pmapcpp.ChunkMap(100, pmapcpp.MapGen.ZEROS)
        chunk = pmap.chunk_at(0, 0)

        # set some cells of chunk to arbitrary values
        pmap.set(7, 8, torch.tensor(0.1))
        pmap.set(0, 0, torch.tensor(0.2))
        pmap.set(99, 0, torch.tensor(0.3))
        pmap.set(99, 99, torch.tensor(0.4))
        pmap.set(0, 99, torch.tensor(0.5))
        pmap.set(88, 46, torch.tensor(0.6))

        # set cells outside of current chunk to make sure new chunks added
        pmap.set(114, 79, torch.tensor(0.7))
        self.assertTrue(pmap.contains_chunk(1, 0))
        pmap.set(-10, -81, torch.tensor(0.8))
        self.assertTrue(pmap.contains_chunk(-1, -1))
        pmap.set(202, 303, torch.tensor(0.9))
        self.assertTrue(pmap.contains_chunk(2, 3))

        # test at() by retrieving cells defined above
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), pmap.at(5, 5))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.1), pmap.at(7, 8))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.2), pmap.at(0, 0))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.3), pmap.at(99, 0))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.4), pmap.at(99, 99))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.5), pmap.at(0, 99))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.6), pmap.at(88, 46))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.7), pmap.at(114, 79))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.8), pmap.at(-10, -81))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.9), pmap.at(202, 303))))

    # Tests stitch_map()
    def test_stitch_map(self):
        pmap = pmapcpp.ChunkMap(100, pmapcpp.MapGen.ZEROS)
        chunk = pmap.chunk_at(0, 0)

        # set some cells of chunk to arbitrary values
        pmap.set(7, 8, torch.tensor(0.1))
        pmap.set(0, 0, torch.tensor(0.2))
        pmap.set(99, 0, torch.tensor(0.3))
        pmap.set(99, 99, torch.tensor(0.4))
        pmap.set(0, 99, torch.tensor(0.5))
        pmap.set(88, 46, torch.tensor(0.6))

        # set cells outside of current chunk to make sure new chunks added
        pmap.set(114, 79, torch.tensor(0.7))
        self.assertTrue(pmap.contains_chunk(1, 0))
        pmap.set(-10, -81, torch.tensor(0.8))
        self.assertTrue(pmap.contains_chunk(-1, -1))
        pmap.set(202, 303, torch.tensor(0.9))
        self.assertTrue(pmap.contains_chunk(2, 3))

        # create chunks that are far away from root chunk
        # get random chunk locations
        randChunkPos1 = (5, 7)
        randChunkPos2 = (79, 2)
        randChunkPos3 = (-45, -7)
        randChunkPos4 = (8, -77)

        # set empty chunks at above locations
        pmap.chunk_set(randChunkPos1[0], randChunkPos1[1], torch.ones(pmap.chunk_size, pmap.chunk_size))
        pmap.chunk_set(randChunkPos2[0], randChunkPos2[1], torch.ones(pmap.chunk_size, pmap.chunk_size))
        pmap.chunk_set(randChunkPos3[0], randChunkPos3[1], torch.ones(pmap.chunk_size, pmap.chunk_size))
        pmap.chunk_set(randChunkPos4[0], randChunkPos4[1], torch.ones(pmap.chunk_size, pmap.chunk_size))
        
        # generate stitched map
        val = 42.0
        stitch, origin = pmap.stitch_map(val_unknown=val)
        #store origin coordinates
        ox = origin[0]
        oy = origin[1]

        # check value that doesn't exist on Chunkmap
        self.assertTrue(torch.all(torch.eq(torch.tensor(val), stitch[800 - ox][-800 - oy])))

        # make sure cells added to original Chunkmap match with cells in stitch
        # ox and oy used to map Chunkmap coordinates to stitch coordinates
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), stitch[5 - ox][5 - oy])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.1), stitch[7 - ox][8 - oy])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.2), stitch[0 - ox][0 - oy])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.3), stitch[99 - ox][0 - oy])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.4), stitch[99 - ox][99 - oy])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.5), stitch[0 - ox][99 - oy])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.6), stitch[88 - ox][46 - oy])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.7), stitch[114 - ox][79 - oy])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.8), stitch[-10 - ox][-81 - oy])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.9), stitch[202 - ox][303 - oy])))
        
        # make sure all values in outer chunks are copied over directly
        for x in range(pmap.chunk_size):
            for y in range(pmap.chunk_size):
                self.assertTrue(torch.all(torch.eq(torch.tensor(1.0), stitch[5*pmap.chunk_size-ox+x][7*pmap.chunk_size-oy+y])))
                self.assertTrue(torch.all(torch.eq(torch.tensor(1.0), stitch[79*pmap.chunk_size-ox+x][2*pmap.chunk_size-oy+y])))
                self.assertTrue(torch.all(torch.eq(torch.tensor(1.0), stitch[-45*pmap.chunk_size-ox+x][-7*pmap.chunk_size-oy+y])))
                self.assertTrue(torch.all(torch.eq(torch.tensor(1.0), stitch[8*pmap.chunk_size-ox+x][-77*pmap.chunk_size-oy+y])))
        
        # create new stitch with optional bounds in place
        stitch_bounded, origin_bounded = pmap.stitch_map(-5, 5, -5, 5, val)
        obx = origin_bounded[0]
        oby = origin_bounded[1]

        # Make sure stitch_bounded is correctly bounded
        self.assertTrue([11*pmap.chunk_size, 11*pmap.chunk_size], list(stitch_bounded.size()))

        # make sure cells added to original Chunkmap match with cells in stitch_bounded
        # obx and oby used to map Chunkmap coordinates to stitch coordinates
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), stitch_bounded[5 - obx][5 - oby])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.1), stitch_bounded[7 - obx][8 - oby])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.2), stitch_bounded[0 - obx][0 - oby])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.3), stitch_bounded[99 - obx][0 - oby])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.4), stitch_bounded[99 - obx][99 - oby])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.5), stitch_bounded[0 - obx][99 - oby])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.6), stitch_bounded[88 - obx][46 - oby])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.7), stitch_bounded[114 - obx][79 - oby])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.8), stitch_bounded[-10 - obx][-81 - oby])))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.9), stitch_bounded[202 - obx][303 - oby])))

    # Tests add_grid()
    def test_add_grid(self):
        pmap = pmapcpp.ChunkMap(100, pmapcpp.MapGen.ZEROS)
        
        #See if add grid works on a single chunk
        pmap.add_grid(25, 25, torch.ones(pmap.chunk_size//2, pmap.chunk_size//2))
        for x in range(25,75):
            for y in range(25, 75):
                self.assertTrue(torch.all(torch.eq(torch.tensor(1.0), pmap.at(x, y))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), pmap.at(24, 50))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), pmap.at(75, 50))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), pmap.at(50, 24))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), pmap.at(50, 75))))

        #see if add_grid makes new chunks
        pmap.add_grid(pmap.chunk_size, pmap.chunk_size, torch.ones(pmap.chunk_size//2, pmap.chunk_size//2))
        self.assertTrue(pmap.contains_chunk(1, 1))
        for x in range(100,150):
            for y in range(100, 150):
                self.assertTrue(torch.all(torch.eq(torch.tensor(1.0), pmap.at(x, y))))

        #see if add_grid can add in overlapping chunck areas
        pmap.add_grid(240, 270, torch.ones(pmap.chunk_size, pmap.chunk_size))
        self.assertTrue(pmap.contains_chunk(2, 2))
        self.assertTrue(pmap.contains_chunk(2, 3))
        self.assertTrue(pmap.contains_chunk(3, 2))
        self.assertTrue(pmap.contains_chunk(3, 3))
        for x in range(240, 340):
            for y in range(270, 370):
                self.assertTrue(torch.all(torch.eq(torch.tensor(1.0), pmap.at(x, y))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), pmap.at(24, 50))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), pmap.at(75, 50))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), pmap.at(50, 24))))
        self.assertTrue(torch.all(torch.eq(torch.tensor(0.0), pmap.at(50, 75))))

        pmap.add_grid(-140, -170, torch.ones(pmap.chunk_size, pmap.chunk_size))
        self.assertTrue(pmap.contains_chunk(-1, -1))
        self.assertTrue(pmap.contains_chunk(-1, -2))
        self.assertTrue(pmap.contains_chunk(-2, -1))
        self.assertTrue(pmap.contains_chunk(-2, -2))
        #print(pmap.chunk_at(-2, -2))
        for x in range(-140, -40):
            for y in range(-170, -70):
                '''if torch.all(torch.eq(torch.tensor(1.0), pmap.at(x, y))):
                    print(x, y)
                    return'''
                self.assertTrue(torch.all(torch.eq(torch.tensor(1.0), pmap.at(x, y))))


        





    '''# Tests gen_empty and get_bounds
    def test_gen_empty_get_bounds(elf):
        pmap = pmapcpp.ChunkMap(100, "empty")

        # generate empty chunks in random locations
        pmap.gen_empty(5, 7)
        pmap.gen_empty(8, 6)
        pmap.gen_empty(2, 1)
        pmap.gen_empty(100, 200)

        # use contains chunk to make sure those chunks are there
        self.assertTrue(pmap.contains_chunk(5, 7))
        self.assertTrue(pmap.contains_chunk(8, 6))
        self.assertTrue(pmap.contains_chunk(2, 1))
        self.assertTrue(pmap.contains_chunk(100, 200))

        # test get bounds when we add those chunks'''








if __name__ == '__main__':
    unittest.main()