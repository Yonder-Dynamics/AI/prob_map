import torch
import cv2
import util
import math
from graph import Pose
from icecream import ic

# This is the data storage class
# Run this file to test the class
# Chunk based map
# Each cell has a logit for the prob that the cell contains the objective, as
# well as a cost for traversal

def sigmoid(x):
    return 1/(1+torch.exp(-x))

class ChunkMap:
    def __init__(self, chunk_size=100, default_gen="empty"):
        self.chunk_size = chunk_size
        self.chunks = { }
        self.default_gen = default_gen

        self.chunks[self.get_ind(0, 0)] = self.gen_empty(0, 0)
        self.cxmin = 0
        self.cymin = 0
        self.cxmax = 0
        self.cymax = 0

    def contains_chunk(self, cx, cy):
        return self.get_ind(cx, cy) in self.chunks

    # TODO: find min/max x, y chunk from obs/qr maps, add max chunks
    def add_grid(self, x, y, grid):
        # figure out which chunks to add to
        w, h = grid.shape
        min_cx = x // self.chunk_size
        min_cy = y // self.chunk_size
        max_cx = (w+x-1) // self.chunk_size
        max_cy = (h+y-1) // self.chunk_size
        offset_x = x % self.chunk_size
        offset_y = y % self.chunk_size
        dest_x = offset_x

        source_x = 0
        for cx in range(min_cx, max_cx+1):
            source_y = 0
            dest_y = offset_y
            for cy in range(min_cy, max_cy+1):
                if not self.contains_chunk(cx, cy):
                    self[cx, cy] = self.gen_empty(cx, cy)

                source_h = min(h - source_y, self.chunk_size-dest_y)
                source_w = min(w - source_x, self.chunk_size-dest_x)
                source = grid[source_x:source_x+source_w, source_y:source_y+source_h]
                self[cx, cy][dest_x:dest_x+source_w, dest_y:dest_y+source_h] += source

                inc_y = self.chunk_size - dest_y
                source_y += inc_y
                dest_y += inc_y
                dest_y %= self.chunk_size # always 0 after first iter?

            inc_x = self.chunk_size - dest_x
            source_x += inc_x
            dest_x += inc_x
            dest_x %= self.chunk_size

    def get_bounds(self):
        return torch.tensor([self.cxmin*self.chunk_size, self.cymin*self.chunk_size]), torch.tensor([(self.cxmax+1)*self.chunk_size-1, (self.cymax+1)*self.chunk_size-1])

    def gen_empty(self, cx, cy):
        if self.default_gen == "empty":
            return torch.zeros((self.chunk_size, self.chunk_size))
        elif self.default_gen == "random":
            return torch.rand(self.chunk_size, self.chunk_size)
        elif self.default_gen == "perlin":
            scale = 5
            lx = torch.linspace(cx*scale,(cx+1)*scale, self.chunk_size, endpoint=True)
            ly = torch.linspace(cy*scale,(cy+1)*scale, self.chunk_size, endpoint=True)
            x,y = torch.meshgrid(lx, ly)
            return util.perlin(x.T, y.T, seed=0)

    # Returns chunk
    def __getitem__(self, cxy):
        cx, cy = cxy
        return self.chunks[self.get_ind(cx, cy)]

    # Sets chunk
    def __setitem__(self, cxy, chunk):
        cx, cy = cxy
        self.chunks[self.get_ind(cx, cy)] = chunk
        # Update
        self.cxmin = min(cx, self.cxmin)
        self.cxmax = max(cx, self.cxmax)
        self.cymin = min(cy, self.cymin)
        self.cymax = max(cy, self.cymax)

    def get_ind(self, cx, cy):
        return "{},{}".format(cx, cy)

    def get_coord(self, ind):
        return [int(i) for i in ind.split(",")]

    def init_chunks(self, cxmin=None, cxmax=None, cymin=None, cymax=None, chunks=None):
        if chunks is None:
            chunks = []
            for x in range(cxmin, cxmax):
                for y in range(cymin, cymax):
                    chunks.append([x, y])
        for (x, y) in chunks:
            self[x, y] = self.gen_empty(x, y)

    # pure
    def gen_map(self, cxmin=None, cxmax=None, cymin=None, cymax=None, val_unknown=0):
        cxmin = self.cxmin if cxmin is None else cxmin
        cxmax = self.cxmax if cxmax is None else cxmax
        cymin = self.cymin if cymin is None else cymin
        cymax = self.cymax if cymax is None else cymax

        out = torch.ones((
            (cxmax-cxmin+1)*self.chunk_size,
            (cymax-cymin+1)*self.chunk_size))*val_unknown

        for key in self.chunks:
            x, y = self.get_coord(key)
            if not ((cxmin <= x <= cxmax) and (cymin <= y <= cymax)):
                continue
            x -= cxmin
            y -= cymin
            out[x*self.chunk_size:(x+1)*self.chunk_size,
                y*self.chunk_size:(y+1)*self.chunk_size] = self.chunks[key]

        return out, torch.tensor([self.cxmin*self.chunk_size, self.cymin*self.chunk_size])

    def random_pose(self):
        min_v, max_v = self.get_bounds()
        scaling = torch.rand(2)
        pos = (max_v-min_v)*scaling + min_v
        ori = torch.rand(1) * 2 * math.pi
        return Pose(pos, ori)

class PMap:
    def __init__(self, chunk_size=100, default_prob_gen="empty", default_obs_gen="empty"):
        self.obs_map = ChunkMap(chunk_size=chunk_size, default_gen=default_obs_gen)
        self.prob_map = ChunkMap(chunk_size=chunk_size, default_gen=default_prob_gen)

    def random_pose(self):
        return self.prob_map.random_pose()

    # check if the obstacle probability along the path is less than the threshold
    def check_path_clear(self, path):
        (min_x, min_y), (max_x, max_y) = self.prob_map.get_bounds()
        for pose in path:
            x, y = pose.position
            if min_x > x or max_x < x or min_y > y or max_y < y:
                return False
        return self.obs_prob(path) < 0.1

    def add_qr(self, qr_x, qr_y, std):
        std = int(std)
        shape = (std*2+1, std*2+1)
        qr_gauss = torch.zeros(shape)
        qr_gauss[std, std] = 1
        qr_gauss = torch.tensor(cv2.GaussianBlur(qr_gauss.numpy(), shape, 0))
        self.prob_map.add_grid(qr_x-std, qr_y-std, qr_gauss)

    def tpr(self, dist):
        return 0.7/dist

    def fpr(self, dist):
        return sigmoid(dist)

    def fpr(self, dist):
        return sigmoid(dist)

    def fpr(self, dist):
        return sigmoid(dist)

    def add_visited_path(self, poses, std):
        std = int(std)
        shape = (std*2+1, std*2+1)
        prob_map, origin = self.prob_map.gen_map()
        (min_x, min_y), (max_x, max_y) = self.prob_map.get_bounds()

        total_prob = 0
        max_prob = 0
        visited_prob_coords = []

        # Compute location and probability of each step of the path
        for i in range(1, len(poses)):
            cells = self.get_grid_cells_btw(poses[i - 1].position - origin, poses[i].position - origin)
            for (prev_x, prev_y), (x, y) in zip(cells[:-1], cells[1:]):
                ix, iy = int(x+0.5), int(y+0.5)
                iprev_x, iprev_y = int(prev_x+0.5), int(prev_y+0.5)
                length = torch.sqrt((x-prev_x)**2 + (y-prev_y)**2)
                # We shouldn't normalize, we should discount
                prob = (prob_map[iprev_x][iprev_y] + prob_map[ix][iy])/2 #*length
                total_prob += prob
                max_prob = max(max_prob, prob)
                visited_prob_coords.append((ix + min_x, iy + min_y, prob))

        # Add uniform dist over current map
        area = (max_x - min_x + 1) * (max_y - min_y + 1)
        uniform = torch.full((max_x - min_x + 1, max_y - min_y + 1), (1.0 + total_prob) / area)
        self.prob_map.add_grid(*origin, uniform)

        # Add negative gaussian of weight p at cell in path
        for x, y, p in visited_prob_coords:
            vis_gauss = torch.zeros(shape)
            vis_gauss[std, std] = -p
            vis_gauss = cv2.GaussianBlur(vis_gauss.numpy(), shape, 0)
            self.prob_map.add_grid(x-std, y-std, torch.tensor(vis_gauss))

    # adding obstacle to probability map (by freshmen :)
    def add_obstacle(self, obs_x, obs_y, std):
        std = int(std)
        shape = (std*2+1, std*2+1)
        obs = torch.zeros(shape) # base torch array of 0s
        obs[std, std] = 1
        obs = cv2.GaussianBlur(obs.numpy(), shape, 0)
        # add distribution to obstacle map
        self.obs_map.add_grid(obs_x, obs_y, torch.tensor(obs))

    def path_prob(self, poses):
        prob_map, origin = self.prob_map.gen_map()
        prob = 0
        for i in range(1, len(poses)):
            cells = self.get_grid_cells_btw(poses[i - 1].position - origin, poses[i].position - origin)
            for (prev_x, prev_y), (x, y) in zip(cells[:-1], cells[1:]):
                ix, iy = int(x+0.5), int(y+0.5)
                iprev_x, iprev_y = int(prev_x+0.5), int(prev_y+0.5)
                length = torch.sqrt((x-prev_x)**2 + (y-prev_y)**2)
                # prob is weighted by the length of how much the path cross through the cells
                prob += (prob_map[iprev_x][iprev_y] + prob_map[ix][iy])/2*length
        return prob

    # determines probability of running into an obstacle on the given path
    def obs_prob(self, poses):
        obs_map, origin = self.obs_map.gen_map()
        obs_prob = 0
        for i in range(1, len(poses)):
            cells = self.get_grid_cells_btw(poses[i - 1].position - origin, poses[i].position - origin)
            # que?
            for (x1, y1), (x2, y2) in zip(cells[:-1], cells[1:]):
                ix1, iy1 = int(x1+0.5), int(y1+0.5)
                ix2, iy2 = int(x2+0.5), int(y2+0.5)
                length = torch.sqrt((x2-x1)**2 + (y2-y1)**2)
                obs_prob += (obs_map[ix1][iy1] + obs_map[ix2][iy2])/2*length
            #  for (x, y) in cells:
            #      ix, iy = int(x+0.5), int(y+0.5)
            #      obs_prob += obs_map[ix][iy]
        return obs_prob


    # https://gamedev.stackexchange.com/a/165295
    def remove_np_duplicates(self, data):
        # Perform lex sort and get sorted data
        sorted_idx = torch.lexsort(data.T)
        sorted_data =  data[sorted_idx,:]
        # Get unique row mask
        row_mask = torch.append([True],torch.any(torch.diff(sorted_data,axis=0),1))
        # Get unique rows
        out = sorted_data[row_mask]
        return out

    def get_grid_cells_btw(self, p1, p2):
        x1,y1 = p1
        x2,y2 = p2
        dx = x2-x1
        dy = y2-y1
        if dx == 0: # will divide by dx later, this will cause err. Catch this case up here
            step = torch.sign(dy)
            ys = torch.arange(0,dy+step,step)
            xs = torch.repeat(x1, ys.shape[0])
        else:
            m = dy/(dx+0.0)
            b = y1 - m * x1

            step = 1.0/(max(abs(dx),abs(dy)))
            xs = torch.arange(x1, x2, step * torch.sign(x2-x1))
            ys = xs * m + b

        pts = torch.stack((xs,ys), dim=1)
        #  pts = self.remove_np_duplicates(pts)

        return pts


if __name__ == "__main__":
    # Test code
    import matplotlib.pyplot as plt
    pmap = PMap()
    #  pmap.init_chunks(cxmax=3, cxmin=0, cymax=3, cymin=0)
    for i in range(100):
        pose = pmap.random_pose()
        x, y = pose.position.int()
        pmap.add_qr(x, y, 25)
    disp, origin = pmap.prob_map.gen_map()
    plt.imshow(disp)
    plt.show()
