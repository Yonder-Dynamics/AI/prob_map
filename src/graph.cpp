
#include "pmap/graph.h"

void init_graph_module(py::module &m) {
  py::class_<Graph>(m, "Graph")
    .def(py::init<Node, int>())
    .def("add_node", &Graph::add_node)
    .def("get_node", &Graph::get_node, py::return_value_policy::reference)
    .def("get_closest_node", &Graph::get_closest_node)
    // .def("update_future_values", &Graph::update_future_values)
    // .def("random_node_i_by_dist", &Graph::random_node_i_by_dist)
    .def("get_closest_nodes", &Graph::get_closest_nodes)
    // .def("extract_best_edge", &Graph::extract_best_edge)
    // .def("end_condition", &Graph::end_condition)
    .def_property_readonly("root", &Graph::root)
    .def("__len__", &Graph::size);
 
  py::class_<Edge>(m, "Edge")
    .def(py::init<int, int, std::vector<Pose>, float>())
    .def("add_pose", &Edge::add_pose)
    .def_readwrite("from_i", &Edge::from_i)
    .def_readwrite("to_i", &Edge::to_i)
    .def_readwrite("base_value", &Edge::base_value)
    .def_readwrite("future_value", &Edge::future_value)
    .def_readwrite("length", &Edge::length)
    .def_readwrite("path", &Edge::path);
  py::class_<Node>(m, "Node")
  //   .def(py::init<float, Pose, int, int>())
  //   .def("add_child", &Node::add_child)
  //   .def("add_edge_ind", &Node::add_edge_ind)
    // .def_readwrite("value", &Node::value)
    .def_readwrite("base_value", &Node::base_value)
    .def_readwrite("cost", &Node::cost)
    .def_readwrite("is_frontier", &Node::is_frontier)
    .def_property_readonly("recursive_cost", &Node::recursive_cost, py::return_value_policy::copy)
    .def("recursive_value", &Node::recursive_value, py::arg("gamma") = 1)
    // .def_readwrite("depth", &Node::depth)
    .def_readwrite("pose", &Node::pose)
    .def_readonly("parent", &Node::parent)
  //   .def_readwrite("edge_indices", &Node::edge_indices)
    .def_readonly("children", &Node::children);
  py::class_<GridMap>(m, "GridMap")
    .def(py::init<int>())
    .def("getNearestNeighbors", &GridMap::getNearestNeighbors)
    .def("insert", &GridMap::insert)
    .def("empty", &GridMap::empty);
}
