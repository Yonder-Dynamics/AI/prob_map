import numpy as np
import torch
from pmapcpp import Node, Graph, Edge, Pose, PMap
import matplotlib.pyplot as plt
# from pmap import PMap
import random
import math

def dist(pose1, pose2):
    # L2 for now
    return torch.norm(pose1.position-pose2.position)

def path_cost(path):
    return dist(path[0], path[-1])

def plan_path(pose1, pose2):
    # Linear path for now
    return [pose1, pose2]

# Closest node
# TODO: Add A* to this
def get_closest_node(graph, pose, max_d):
    closest_i = -1
    closest_dist = 99999999
    closest_d = 0
    for i, node in enumerate(graph.nodes):
        d = dist(node.pose, pose)
        if d > max_d: continue
        segment_dist = node.dist
        # segment_dist = (node.dist * 1.0) / (node.depth + 1)
        if closest_dist > d + segment_dist:
        # if closest_dist > d:
            closest_dist = d + segment_dist
            closest_d = d
            closest_i = i
    return closest_i, closest_d

def random_pose(graph, obs_cache_map, origin, center_dist, max_d=50):
    # Sample pose in map
    # Pick a random node from the graph
    #  si = graph.random_node_i_by_dist()
    # si = random.randint(len(graph.nodes)//2, len(graph.nodes)-1)
    si = 0
    start = graph.nodes[si].pose

    #  scaling = 2*np.random.rand(2)-1
    #  l = np.sqrt((scaling**2).sum())
    t = torch.rand(1) * 2 * math.pi
    #  r = torch.sqrt(torch.rand(1))*max_d + 5
    r = torch.sqrt(torch.rand(1))*center_dist
    #  scaling = np.random.normal([0, 0], [center_dist, center_dist])
    offset = torch.tensor([r*torch.cos(t), r*torch.sin(t)]).T.squeeze()
    pos = start.position+offset
    ori = torch.rand(1) * 2 * math.pi

    # Pick a node the max_d away
    pose = Pose(pos, ori)
    # check if node is clear before 
    nij = (pos-origin).int()
    if nij[0] < 0 or nij[1] < 0 or nij[0] >= obs_cache_map.shape[0] or nij[1] >= obs_cache_map.shape[1]:
        return -1, None
    if obs_cache_map[nij[0], nij[1]] > 0.1:
        return -1, None
    return 0, pose


def add_spokes(graph, pmap, obs_cache_map, origin, radius, num_spoke=20):
    for angi in torch.linspace(0, 2*math.pi, num_spoke+1)[1:]:
        offset = torch.tensor([radius*torch.cos(angi), radius*torch.sin(angi)]).T.squeeze()
        pos = graph.nodes[0].pose.position+offset
        pose = Pose(pos, angi)
        add_path(pose, 0, graph, pmap, obs_cache_map, origin)


def iterate_rrt(graph, pmap, obs_cache_map, origin, center_dist, max_d=50):
    err, pose = random_pose(graph, obs_cache_map, origin, center_dist)
    if err != 0:
        return -2
    i, d = graph.get_closest_nodes(pose, 1)[0]
    #  i, d = get_closest_node(graph, pose, max_d)
    if i == -1:
        return -1
    return add_path(pose, i, graph, pmap, obs_cache_map, origin)

def add_path(pose, node_i, graph, pmap, obs_cache_map, origin):
    path = plan_path(pose, graph.nodes[node_i].pose.clone())
    # check probability of path being clear
    if not pmap.check_path_clear(path, obs_cache_map, origin, 0.1):
        return -1
    j = len(graph.nodes)
    e_idx = len(graph.edges)
    graph.nodes[node_i].add_edge_ind(e_idx)
    graph.nodes[node_i].add_child(j)
    n = Node(0, pose.clone(), graph.nodes[node_i].depth + 1, node_i)
    #  n.dist = dist(pose, graph.nodes[0].pose) + graph.nodes[node_i].dist
    n.dist = dist(pose, graph.nodes[0].pose)
    graph.add_node(n)
    graph.add_edge(Edge(node_i, j, path, path_cost(path)))
    return 0

if __name__ == "__main__":
    pmap = PMap(10)
    pmap.add_qr(20, 50)
    pmap.add_qr(70, 0)
    pmap.add_qr(-10, -10)
    pmap.add_qr(-20, -50)
    pmap.add_obstacle(20, 0)

    N = 20
    i = 0
    graph = Graph()
    root_pos = Pose(torch.tensor([0.5, 0.5]), torch.tensor([0]))
    root = Node(0, root_pos, 0, None)
    graph.nodes.append(root)
    while i < N:
        if iterate_rrt(graph, pmap) == 0:
            i += 1

    # Gather nodes to plot
    for e in graph.edges:
        p1 = graph.nodes[e.from_i].pose.position
        p2 = graph.nodes[e.to_i].pose.position
        plt.plot(*zip(*[p1, p2]))
    plt.show()
