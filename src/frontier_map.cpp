#include <torch/extension.h>
#include <unordered_map>
#include <utility>
#include <vector>

#include "pmap/frontier_map.h"
#include "pmap/kmeans.h"

void init_frontier_module(py::module &m) {
  py::class_<FrontierMap>(m, "FrontierMap")
    .def(py::init<int>())
    .def_readwrite("frontier_inds", &FrontierMap::frontier_inds)
    .def_readwrite("num_frontiers", &FrontierMap::num_frontiers);
}

bool
FrontierMap::is_frontier(Coord &c, FrontierContext *ctx) {
    // Needs to be run after the observation has already been used to update the map
    // An unknown region that has at least one open space neighbor
    bool has_open = false;
    for (Coord neighbor_rel: NEIGHBORS) {
        Coord neighbor = c + neighbor_rel;
        // has_unknown |= ctx->is_unknown(neighbor);
        has_open |= ctx->is_free(neighbor);
    }
    return has_open && ctx->is_unknown(c) && ctx->is_free(c);
}

void
FrontierMap::delete_frontier(int x, int y) {
    int ind = frontier_inds.at(x, y).item<int>();
    Coord c = std::make_pair(x, y);
    if (ind == -1) {
        return;
    }
    frontier_coords[ind].remove(c);
    frontier_inds.set(x, y, torch::tensor(-1));
}

void
FrontierMap::add_frontiers(std::vector<Coord> &contour, FrontierContext *ctx) {
    // First, add new frontiers
    bool prev_frontier = false;
    std::unordered_map<int, int, IntHash> new_frontier_sizes;
    int frontier_ind = num_frontiers;
    for (size_t i=0; i<contour.size(); i++) {
        Coord &c = contour[i];

        if (!is_frontier(contour[i], ctx)) {
            prev_frontier = false;
            continue;
        }
        if (!prev_frontier) {
            frontier_ind++;
        }
        // Add the frontier
        frontier_coords[frontier_ind].push_back(c);
        // REMOVE
        // frontier_inds.set(c.first, c.second, torch::tensor(frontier_ind));
        new_frontier_sizes[frontier_ind]++;
        num_frontiers++;

        prev_frontier = true;
    }
    // Second, merge frontiers
    int frontier_size;
    for (auto &frontier: new_frontier_sizes) {
        std::tie(frontier_ind, frontier_size) = frontier;
        // Retrieve all overlapping frontiers
        std::set<int> overlapping_frontiers;
        for (auto &c: frontier_coords[frontier_ind]) {
            // I could traverse a neighborhood here
            for (Coord neighbor_rel: NEIGHBORS) {
                Coord neighbor = c + neighbor_rel;
                int neighbor_ind = frontier_inds.at(neighbor.first, neighbor.second).item<int>();
                if (neighbor_ind != -1) {
                    overlapping_frontiers.insert(neighbor_ind);
                }
            }
            // int last_frontier_ind = frontier_inds.at(c.first, c.second).item<int>();
            // if (last_frontier_ind != -1) {
            //     overlapping_frontiers.insert(last_frontier_ind);
            // }
        }

        // Handle the amount of overlap
        if (overlapping_frontiers.size() == 1) {
            // Just merge the new frontier we have into the existing frontier
            int overlapping_ind = *overlapping_frontiers.begin();
            for (auto &c: frontier_coords[frontier_ind]) {
                frontier_inds.set(c.first, c.second, torch::tensor(overlapping_ind));
                frontier_coords[overlapping_ind].push_back(c);
            }
            frontier_coords.erase(frontier_ind);
        } else if (overlapping_frontiers.size() > 1) {
            // We have to merge all the frontiers we found into the lowest frontier
            // Aggregate frontier coords
            int overlapping_ind = *overlapping_frontiers.begin();
            auto iter = ++overlapping_frontiers.begin();
            for (; iter != overlapping_frontiers.end(); iter++) {
                int ind = *iter;
                for (auto &c: frontier_coords[ind]) {
                    frontier_inds.set(c.first, c.second, torch::tensor(overlapping_ind));
                    frontier_coords[overlapping_ind].push_back(c);
                }
                frontier_coords.erase(ind);
            }
            for (auto &c: frontier_coords[frontier_ind]) {
                frontier_inds.set(c.first, c.second, torch::tensor(overlapping_ind));
            }
        } else {
            // No overlapping frontiers, so we can just add the frontier inds to the map
            for (auto &c: frontier_coords[frontier_ind]) {
                frontier_inds.set(c.first, c.second, torch::tensor(frontier_ind));
            }
        }
    }
    num_frontiers = 0;
    for (auto &frontier: frontier_coords) {
        frontier_ind = frontier.first;
        num_frontiers = std::max(num_frontiers, frontier_ind);
    }
}

std::vector<Node>
FrontierMap::generate_clusters(int pts_per_cluster, float remaining_prob,
        std::function< float(Coord&, float) >weight_func) {
    // There is some kind of function that generates a value given the size of the frontier associated witht the cluster
    // If I use the mean centroid, I will have issues when the frontier is large
    // I can just use K mean clustering with a heuristic to determine the number of clusters
    // This heuristic can be based on the number of cells in the frontier being clustered
    std::vector<Node> cluster_nodes;

    int frontier_ind;
    std::vector<Coord> ind_coords;
    std::list<Coord> ind_coords_list;
    ClusterInds cluster_inds;
    std::vector<Coord> clusters;

    // First, count number of frontier cells so the percent unknown can be calculated
    size_t num_frontier_cells = 0;
    for (auto &c: frontier_coords) {
        num_frontier_cells += c.second.size();
    }

    float nu = 0;

    for (auto &c: frontier_coords) {
        std::tie(frontier_ind, ind_coords_list) = c;
        // Convert ind_coords_list to vector
        ind_coords.clear();
        std::copy(ind_coords_list.begin(), ind_coords_list.end(), std::back_inserter(ind_coords));

        int num_clusters = ceil(ind_coords.size() / (float)pts_per_cluster);
        std::tie(cluster_inds, clusters) = kmeans(num_clusters, ind_coords);
        for (size_t i=0; i<clusters.size(); i++) {
            float percent = cluster_inds[i].size() / ((float)num_frontier_cells+1e-6);
            float val = weight_func(clusters[i], percent);
            // This could be real fucking fancy
            Pose p (clusters[i].first, clusters[i].second, 0);
            Node n (p, NULL);
            n.base_value = val;
            n.is_frontier = true;
            cluster_nodes.push_back(n);
            nu += val;
        }
    }
    // Normalize cluster base_values to be in line with the remaining_prob
    for (auto &n: cluster_nodes) {
        n.base_value = n.base_value/(nu+1e-8)*remaining_prob;
    }
    return cluster_nodes;
}