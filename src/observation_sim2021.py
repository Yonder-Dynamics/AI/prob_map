import matplotlib.pyplot as plt
import torch
import math
import util
import numpy as np

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

class SensorModel:
    def __init__(self, fx, cx, max_dist, res, optimal_range, min_accuracy, background_noise):
        # optimal_range [min, max]
        self.optimal_range = optimal_range
        self.min_accuracy = min_accuracy
        self.background_noise = background_noise
        self.fx = fx
        self.cx = cx
        self.max_dist = max_dist
        self.res = res

    @property
    def fov(self):
        return np.arctan(self.cx/self.res*2)

    def prob_det_given_true(self, distance):
        # accuracy as a value between 0 and 1
        return 1-(1-self.min_accuracy)*sigmoid((distance-self.optimal_range[0])/(self.optimal_range[1]-self.optimal_range[0]))

    def prob_det_given_false(self, distance):
        # accuracy as a value between 0 and 1
        return (self.min_accuracy)*sigmoid((distance-self.optimal_range[0])/(self.optimal_range[1]-self.optimal_range[0]))

    def prob_det_given_nothing(self, distance):
        # accuracy as a value between 0 and 1
        return (self.background_noise)*sigmoid((distance-self.optimal_range[0])/(self.optimal_range[1]-self.optimal_range[0]))

    def conf_from_prob(self, prob, n=1):
        # return (prob/2)*torch.rand(n) + prob/2
        # return (torch.rand(n) < prob/2).float()
        return torch.as_tensor(np.random.normal(prob, (1-prob)/4, n)).clip(0, 1).float()

class ObservationSim:
    def __init__(self, sensor_model, position_sigma=0, real_tags=[], fake_tags=[], tag_size=2):
        # array of poses
        # fx, cx are instrinsic camera parameters
        self.real_tags = real_tags
        self.fake_tags = fake_tags
        self.sensor_model = sensor_model
        self.tag_size = tag_size
        self.position_sigma = position_sigma

    def gen_random_tags(self, n, minx, maxx, miny, maxy):
        tags = torch.rand(n, 2)
        tags[:, 0] = tags[:, 0] * (maxx-minx) + minx
        tags[:, 1] = tags[:, 1] * (maxy-miny) + miny
        return tags

    def graph(self):
        rx, ry = zip(*[xy for xy in self.real_tags])
        fx, fy = zip(*[xy for xy in self.fake_tags])
        plt.scatter(rx, ry, marker="*", color="g")
        plt.scatter(fx, fy, marker="x", color="r")
        # plt.scatter(self.real_tags[:, 0], self.real_tags[:, 1], marker="*", color="g")
        # plt.scatter(self.fake_tags[:, 0], self.fake_tags[:, 1], marker="x", color="r")

    def back_proj(self, tag, pose):
        diff = tag - pose.position + np.random.normal(0, self.position_sigma)
        # rotate ray to current position
        ray = util.rot(-pose.orientation+math.pi/2) @ diff.reshape(2, 1)
        # cast a ray into the pose
        dist = ray[1]
        i = int((ray[0]-self.tag_size) * self.sensor_model.fx / dist + self.sensor_model.cx + 0.5)
        j = int((ray[0]+self.tag_size) * self.sensor_model.fx / dist + self.sensor_model.cx + 0.5)
        i = max(0, i)
        j = min(self.sensor_model.res-1, j)
        # add that to the observation array
        if dist < 0 or i > j:
            return None, -1, -1
        return dist, i, j

    def gen_observation(self, pose, pmap, safety_radius=0.5):
        #  obs_cache_map, obs_origin = pmap.obs_map.stitch_map()
        obs = torch.ones((self.sensor_model.res, 2))
        c = self.sensor_model.prob_det_given_nothing(self.sensor_model.max_dist)
        obs[:, 0] = self.sensor_model.conf_from_prob(c, n=self.sensor_model.res)
        obs[:, 1] *= self.sensor_model.max_dist
        for i in range(self.sensor_model.res):
            diff = torch.tensor([(i-self.sensor_model.cx)/self.sensor_model.fx, 1])
            ray = util.rot(pose.orientation-math.pi/2) @ diff.reshape(2, 1)
            #  d = pmap.dist_to_obstacle(pose, ray.reshape(2), obs_cache_map, obs_origin, self.max_dist, 0.1);
            dist = pmap.dist_to_obstacle(pose, ray.reshape(2), self.sensor_model.max_dist);
            c = self.sensor_model.prob_det_given_nothing(dist)
            obs[i, 0] = self.sensor_model.conf_from_prob(c)
            obs[i, 1] = max(dist, 0)

        # for each real/fake tag
        for tag in self.real_tags:
            dist, i, j = self.back_proj(tag, pose)
            if dist is None:
                continue
            mask = dist < obs[i:j, 1]
            c = self.sensor_model.prob_det_given_true(dist)
            if mask.sum() > 0:
                print("Saw real", c)
            obs[i:j, :][mask, 0] = self.sensor_model.conf_from_prob(c)
            obs[i:j, :][mask, 1] = dist

        for tag in self.fake_tags:
            dist, i, j = self.back_proj(tag, pose)
            if dist is None:
                continue
            mask = dist < obs[i:j, 1]
            c = self.sensor_model.prob_det_given_false(dist)
            if mask.sum() > 0:
                print("Saw fake: ", c)
            obs[i:j, :][mask, 0] = self.sensor_model.conf_from_prob(c)
            obs[i:j, :][mask, 1] = dist
        print(obs[:, 0])
        return obs
