#include <torch/extension.h>

#include <utility>
#include <vector>
#include <iostream>
#include <fstream>

#include <unordered_map>

#include "ATen/core/TensorBody.h"
#include "c10/core/ScalarType.h"
#include "pmap/chunkmap.h"
#include "pmap/utils.h"

void init_chunkmap_module(py::module &m) {
  py::enum_<MapGen>(m, "MapGen", py::arithmetic(), "Generator types for gen_empty in ChunkMap")
    .value("ZEROS", MapGen::ZEROS, "Zero init")
    .value("ONES", MapGen::ONES, "One init")
    .export_values();
  py::class_<Chunk>(m, "Chunk")
    .def_readwrite("data", &Chunk::data)
    .def_readwrite("tag", &Chunk::tag);
  py::class_<ChunkMap>(m, "ChunkMap")
    .def(py::init<int, MapGen>(),
        py::arg("chunk_size") = 100,
        py::arg("default_gen") = MapGen::ZEROS
        )
    .def("gen_empty", &ChunkMap::gen_empty)
    .def("contains_chunk", &ChunkMap::contains_chunk)
    .def("get_bounds", &ChunkMap::get_bounds)
    .def("random_pose", &ChunkMap::random_pose)
    .def("chunk_set", &ChunkMap::chunk_set)
    .def("chunk_at", &ChunkMap::chunk_at)
    .def("stitch_map", &ChunkMap::stitch_map,
        py::arg("lcxmin") = py::none(),
        py::arg("lcxmax") = py::none(),
        py::arg("lcymin") = py::none(),
        py::arg("lcymax") = py::none()
        )
    .def("add_grid", &ChunkMap::add_grid)
    .def("at", &ChunkMap::at)
    .def("set", &ChunkMap::set)
    .def_readwrite("chunk_size", &ChunkMap::chunk_size)
    .def_readwrite("chunks", &ChunkMap::chunks)
    .def_readwrite("default_gen", &ChunkMap::default_gen)
    .def_readwrite("cxmin", &ChunkMap::cxmin)
    .def_readwrite("cymin", &ChunkMap::cymin)
    .def_readwrite("cxmax", &ChunkMap::cxmax)
    .def_readwrite("cymax", &ChunkMap::cymax);
}

ChunkMap::ChunkMap(int chunk_size, MapGen default_gen) 
  : chunk_size(chunk_size), default_gen(default_gen),
    cxmin(0), cymin(0), cxmax(0), cymax(0)
{
  chunks[std::make_pair(0, 0)] = gen_empty();
}

bool
ChunkMap::contains_chunk(int cx, int cy)
{
  return !( chunks.find(std::make_pair(cx, cy)) == chunks.end() );
}

std::tuple<torch::Tensor, torch::Tensor>
ChunkMap::get_bounds()
{
  return std::make_tuple<torch::Tensor, torch::Tensor>(
      torch::tensor({cxmin*chunk_size, cymin*chunk_size}),
      torch::tensor({(cxmax+1)*chunk_size-1, (cymax+1)*chunk_size-1}));
}

Chunk
ChunkMap::gen_empty()
{
  switch (default_gen) {
    case MapGen::ZEROS:
      return Chunk{.data=torch::zeros({chunk_size, chunk_size}, torch::kFloat32), .tag=false};
      break;
    case MapGen::ONES:
      return Chunk{.data=torch::ones({chunk_size, chunk_size}, torch::kFloat32), .tag=false};
      break;
    case MapGen::NEG_ONES_INT:
      return Chunk{.data=-torch::ones({chunk_size, chunk_size}, torch::kInt), .tag=false};
      break;
    default:
      return Chunk{.data=torch::zeros({chunk_size, chunk_size}, torch::kFloat32), .tag=false};
      break;
  }
}

torch::Tensor
ChunkMap::default_value()
{
  switch (default_gen) {
    case MapGen::ZEROS:
      return torch::tensor(0, torch::kFloat32);
    case MapGen::ONES:
      return torch::tensor(1, torch::kFloat32);
      break;
    case MapGen::NEG_ONES_INT:
      return torch::tensor(-1, torch::kInt);
      break;
    default:
      return torch::tensor(0, torch::kFloat32);
      break;
  }
}
torch::Tensor 
ChunkMap::chunk_at(int cx, int cy)
{
  return chunks[std::make_pair(cx, cy)].data;
}

void 
ChunkMap::chunk_set(int cx, int cy, Chunk chunk)
{
  chunks[std::make_pair(cx, cy)] = chunk;
  cxmin = std::min(cx, cxmin);
  cxmax = std::max(cx, cxmax);
  cymin = std::min(cy, cymin);
  cymax = std::max(cy, cymax);
}

torch::Tensor
ChunkMap::at(int x, int y)
{
  int cx = floor(float(x) / chunk_size);
  int cy = floor(float(y) / chunk_size);
  if (!contains_chunk(cx, cy)) {
    // return torch::tensor(CMAP_NIL);
    chunk_set(cx, cy, gen_empty());
  }
  return chunks[std::make_pair(cx, cy)].data[x % chunk_size][y % chunk_size];
}

void
ChunkMap::set(int x, int y, torch::Tensor value)
{
  int cx = floor(float(x) / chunk_size);
  int cy = floor(float(y) / chunk_size);
  if (!contains_chunk(cx, cy)) {
    chunk_set(cx, cy, gen_empty());
  }
  chunks[std::make_pair(cx, cy)].data[x % chunk_size][y % chunk_size] = value;
}

torch::Tensor
ChunkMap::sum()
{
  torch::Tensor sum = torch::zeros_like(default_value());
  for (auto it : this->chunks) {
    sum += it.second.data.sum();
  }
  return sum;
}

void
ChunkMap::operator*=(torch::Tensor other)
{
  for (auto it : this->chunks) {
    it.second.data *= other;
  }
}

std::tuple<torch::Tensor, torch::Tensor>
ChunkMap::stitch_map(c10::optional<int> pcxmin,
                     c10::optional<int> pcxmax,
                     c10::optional<int> pcymin,
                     c10::optional<int> pcymax)
{
  int lcxmin = pcxmin.value_or(cxmin);
  int lcxmax = pcxmax.value_or(cxmax);
  int lcymin = pcymin.value_or(cymin);
  int lcymax = pcymax.value_or(cymax);

  torch::Tensor out = torch::ones({
        (lcxmax-lcxmin+1)*chunk_size,
        (lcymax-lcymin+1)*chunk_size
      })*default_value();
  for (auto it : this->chunks) {
    int x, y;
    std::tie(x, y) = it.first;
    if (!((lcxmin <= x && x <= lcxmax) && (lcymin <= y && y <= lcymax))) {
      continue;
    }
    // changed from cxmin and cymin to lcxmin and lcymin
    x -= lcxmin;
    y -= lcymin;
    for (int i=0; i<chunk_size; i++) {
      for (int j=0; j<chunk_size; j++) {
        out[x*chunk_size + i][y*chunk_size + j] = it.second.data[i][j];
      }
    }
  }
  torch::Tensor origin = torch::tensor({lcxmin*chunk_size, lcymin*chunk_size});
  return std::make_tuple<torch::Tensor, torch::Tensor>(std::move(out), std::move(origin));
}

void
ChunkMap::add_grid(int x, int y, torch::Tensor grid)
{
  // figure out which chunks to add to
  int w = grid.size(0);
  int h = grid.size(1);
  // changed min_cx, min_cy, max_cx, max_cy calculation to allow for negative chunks
  int min_cx = floor(float(x) / chunk_size);
  int min_cy = floor(float(y) / chunk_size);
  int max_cx = floor(float(w+x-1) / chunk_size);
  int max_cy = floor(float(h+y-1) / chunk_size);
  //int offset_x = x % chunk_size;
  //int offset_y = y % chunk_size;
  //int dest_x = offset_x;

  for (int cx=min_cx; cx<max_cx+1; cx++) {
    //int source_y = 0;
    //int dest_y = offset_y;
    for (int cy=min_cy; cy<max_cy+1; cy++) {
      if (!contains_chunk(cx, cy)) {
        chunk_set(cx, cy, gen_empty());
      }
    }
  }

  for (int ix = x; ix < x + w; ix++){
    int cx = floor(float(ix) / chunk_size);
    for (int iy = y; iy < y + w; iy++){
      int cy = floor(float(iy) / chunk_size);
      chunks[std::make_pair(cx, cy)].data[ix % chunk_size][iy % chunk_size] += grid[ix-x][iy-y];
    }
  }

  /*int source_x = 0;
  for (int cx=min_cx; cx<max_cx+1; cx++) {
    int source_y = 0;
    int dest_y = offset_y;
    for (int cy=min_cy; cy<max_cy+1; cy++) {
      if (!contains_chunk(cx, cy)) {
        chunk_set(cx, cy, gen_empty());
      }

      // used absolute value to get magnitude instead of actual value
      int source_h = std::min(h - source_y, chunk_size-dest_y);
      int source_w = std::min(w - source_x, chunk_size-dest_x);
      for (int i=0; i<source_w; i++) {
        for (int j=0; j<source_h; j++) {
          chunk_at(cx, cy)[dest_x+i][dest_y+j] += grid[source_x+i][source_y+j];
        }
      }

      int inc_y = chunk_size - dest_y;
      source_y += inc_y;
      dest_y += inc_y;
      dest_y %= chunk_size;
    }
    int inc_x = chunk_size - dest_x;
    source_x += inc_x;
    dest_x += inc_x;
    dest_x %= chunk_size;
  }*/
}

torch::Tensor
ChunkMap::random_pose()
{
  // Only handles rectangular maps
  torch::Tensor min_v, max_v;
  std::tie(min_v, max_v) = get_bounds();
  torch::Tensor scaling = torch::rand(2);
  torch::Tensor pose = torch::zeros({3});
  pose[0] = (max_v[0] - min_v[0])*scaling[0] + min_v[0];
  pose[1] = (max_v[1] - min_v[1])*scaling[1] + min_v[1];
  pose[2] = torch::rand(1)[0] * 2 * M_PI;
  return pose;
}

// bool ChunkMap::tag_at(int cx, int cy) {
//   Coord c = std::make_pair(cx, cy);
//   return chunks[c].tag;
// }

