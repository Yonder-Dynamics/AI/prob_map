#include "octree/cuda_raycaster.h"
#include "octree/octree.h"

#include <iostream>
#include <cstring>


int main(int argc, char** argv) {
  if (argc < 2) {
    printf("Usage: vis path_to_octomap\n");
    return -1;
  }
  std::string path = argv[1];
  std::cout << "Loading octree from: " << path << std::endl;
  octree::Octree tree (path);
  printf("Depth: %i\n", tree.current_max_depth);
  printf("Resolution: %f\n", tree.scale);

  octree::CameraConfig cfg = {
    .transform_matrix={
      1, 0, 0, 0.000000,
      0, 0, 1, -1.134990,
      0, -1, 0, -2.151347,
      0, 0, 0, 1
    }
  };
  int w = 512;
  int h = 512;
  cfg.im_cols = w;
  cfg.im_rows = h;
  cfg.cx = w/2;
  cfg.cy = h/2;
  cfg.fx = w/2;
  cfg.fy = w/2;

  float * image = new float[cfg.im_cols * cfg.im_rows];
  float * image2 = new float[cfg.im_cols * cfg.im_rows];

  octree::CudaRaycaster caster(tree, cfg);;
  caster.renderDepth(cfg, image);
  caster.projectScore(cfg, image);
  memset(image, 0, sizeof(float)*cfg.im_cols * cfg.im_rows);
  caster.renderScoreCount(cfg, image, image2);

  float cmax = -9999;
  float cmin = 9999;
  for (int i=0; i<cfg.im_rows; i++) {
    for (int j=0; j<cfg.im_cols; j++) {
      size_t ind = i*cfg.im_cols + j;
      float val = image[ind];
      // int64_t index = index_image[ind];
      // float val = indexToCol(index);
      cmax = std::max(cmax, val);
      cmin = std::min(cmin, val);
    }
  }

  std::cout << "P3\n" << cfg.im_cols << " " << cfg.im_rows << "\n255\n";
  for (int i=0; i<cfg.im_rows; i++) {
    for (int j=0; j<cfg.im_cols; j++) {
      float val = image[i*cfg.im_cols + j];
      val = (val + cmin)/(cmax-cmin);
      std::cout << int(val*255) << " ";
      std::cout << int(val*255) << " ";
      std::cout << int(val*255) << "\n";
    }
  }
  return 0;
}

