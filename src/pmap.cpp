#include <algorithm>
#include <torch/extension.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <limits>
#include <math.h>

#include <unordered_map>

#include "pmap/geom_util.h"
#include "pmap/pmap.h"
#include "pmap/utils.h"
#include "pmap/graph.h"
#define debug false

namespace F = torch::nn::functional;

void init_pmap_module(py::module &m) {
  py::class_<PMap>(m, "PMap")
    .def(py::init<float, float, int, MapGen, MapGen>(),
         py::arg("obs_threshold"),
         py::arg("res") = 0.1,
         py::arg("chunk_size") = 100,
         py::arg("default_prob_gen") = MapGen::ZEROS,
         py::arg("default_obs_gen") = MapGen::ZEROS
         )
    .def("random_pose", &PMap::random_pose)
    .def("check_path_clear", &PMap::check_path_clear)
    .def("dist_to_obstacle", &PMap::dist_to_obstacle)
    .def("add_qr", &PMap::add_qr)
    .def("test", &PMap::test)
    .def("tpr", &PMap::tpr)
    .def("fpr", &PMap::fpr)
    .def("tnr", &PMap::tnr)
    .def("fnr", &PMap::fnr)
    .def("add_visited_path", &PMap::add_visited_path)
    .def("add_observation", &PMap::add_observation)
    .def("add_obstacle", &PMap::add_obstacle)
    .def("obs_prob", &PMap::obs_prob)
    .def("path_prob", &PMap::path_prob)
    .def("get_grid_cells_btw", &PMap::get_grid_cells_btw)
    .def("add_uniform", &PMap::add_uniform)
    .def_readwrite("obs_threshold", &PMap::obs_threshold)
    .def_readwrite("base_tag_prob_map", &PMap::base_tag_prob_map)
    .def_readwrite("frontier_map", &PMap::frontier_map)
    .def_readwrite("known_map", &PMap::known_map)
    .def_readwrite("TP_map", &PMap::TP_map)
    .def_readwrite("TN_map", &PMap::TN_map)
    .def_readwrite("FP_map", &PMap::FP_map)
    .def_readwrite("FN_map", &PMap::FN_map)
    .def_readwrite("prob_map", &PMap::prob_map)
    .def_readwrite("obs_map", &PMap::obs_map);
}

PMap::PMap(float obs_threshold, float map_res, int chunk_size, MapGen default_prob_gen, MapGen default_obs_gen) :
  chunk_size(chunk_size), obs_threshold(obs_threshold), known_map(chunk_size, MapGen::NEG_ONES_INT), frontier_map(chunk_size), map_res(map_res)
{
  this->obs_map = ChunkMap(chunk_size, default_obs_gen);
  this->prob_map = ChunkMap(chunk_size, default_prob_gen);
  this->base_tag_prob_map = ChunkMap(chunk_size, default_prob_gen);
  this->TP_map = ChunkMap(chunk_size, MapGen::ZEROS);
  this->FP_map = ChunkMap(chunk_size, MapGen::ZEROS);
  this->TN_map = ChunkMap(chunk_size, MapGen::ZEROS);
  this->FN_map = ChunkMap(chunk_size, MapGen::ZEROS);
  this->obs_counter = ChunkMap(chunk_size, MapGen::ZEROS);
}

torch::Tensor PMap::random_pose()
{
  return this->obs_map.random_pose();
}

bool PMap::check_path_clear(std::vector<Pose> path)
{
  torch::NoGradGuard guard;
  torch::Tensor minv, maxv;
  std::tie(minv, maxv) = this->obs_map.get_bounds();
  for (Pose pose: path) {
      if (((minv[0] > pose.x) |
          (maxv[0] < pose.x) |
          (minv[1] > pose.y) |
          (maxv[1] < pose.y)).item<bool>()) {
          return false;
      }
  }
  return this->obs_prob(path) < obs_threshold;
}

float PMap::tpr(float dist) { return 0.5; }
float PMap::fpr(float dist) { return 0.1; }
float PMap::tnr(float dist) { return 0.3; }
float PMap::fnr(float dist) { return 0.1; }

void PMap::add_visited_path(std::vector<Pose> path, std::vector<torch::Tensor> observations, float fx, float cx)
{
  for (size_t i=0; i<path.size(); i++) {
    add_observation(path[i], observations[i], fx, cx);
  }
}

float PMap::dist_to_obstacle(Pose & pose, torch::Tensor ray, float max_d) {
  std::vector<Coord> line;
  create_line(
    line,
    pose.x,
    pose.y,
    pose.x + (ray*max_d)[0].item<float>(),
    pose.y + (ray*max_d)[1].item<float>());

  // float norm = torch::linalg::norm(ray).item<float>();
  int x, y;
  for (float d=0; d<max_d; d+=map_res/4) {
    torch::Tensor pos = pose.position()+ray*d;
    x = pos[0].item<float>();
    y = pos[1].item<float>();
    float v = obs_map.at(x, y).item<float>();
    if (v == CMAP_NIL) {
      return d;
    }

    // float v = tens_map[x][y].item<float>();
    // if (x < 0 || y < 0 || x >= tens_map.size(0) || y >= tens_map.size(1)) {
    //   return d;
    // }
    if (v > obs_threshold) {
      return d;
    }
  }
  return max_d;
}

float PMap::path_prob(Pose & pose1, Pose & pose2, float max_d, float fov) {
  if (pose1.x == pose2.x && pose1.y == pose2.y) {
    return 0.0;
  }
  // Goes from pose1 to pose2
  float prob = 0;

  std::vector<Coord> left_ray, right_ray;
  std::vector<Coord> center, rays, end;
  float line_angle = atan2(pose2.y - pose1.y, pose2.x - pose1.x);
  float len = std::sqrt(std::pow(pose1.x-pose2.x, 2) + std::pow(pose1.y-pose2.y, 2));

  create_line(left_ray, 0, 0, cos(fov/2+line_angle)*max_d, sin(fov/2+line_angle)*max_d);
  create_line(right_ray, 0, 0, cos(-fov/2+line_angle)*max_d, sin(-fov/2+line_angle)*max_d);
  create_line(center, (int)pose1.x, (int)pose1.y, (int)pose2.x, (int)pose2.y);
  create_line(end, (int)pose2.x, (int)pose2.y, pose2.x + max_d/len*(pose2.x-pose1.x), pose2.y + max_d/len*(pose2.y-pose1.y));
  // Ensure lines are going in the correct direction
  if (left_ray.begin()->first != 0 || left_ray.begin()->second != 0) {
    std::reverse(left_ray.begin(), left_ray.end());
  }
  if (right_ray.begin()->first != 0 || right_ray.begin()->second != 0) {
    std::reverse(right_ray.begin(), right_ray.end());
  }
  if (center.begin()->first != (int)pose1.y || center.begin()->second != (int)pose1.y) {
    std::reverse(center.begin(), center.end());
  }
  if (end.begin()->first != (int)pose2.x || end.begin()->second != (int)pose2.y) {
    std::reverse(end.begin(), end.end());
  }

  // fprintf(stderr, "size: %zu\n", center.size());
  // First, fill the left and right of the center of the line
  for(auto point: center) {
    // std::cout << "boo" << left_ray.size() << ", " << right_ray.size() << std::endl;
    int &ox = point.first;
    int &oy = point.second;
    // add values from center of line
    prob += prob_map.at(ox, oy).item<float>();
    float v = obs_map.at(ox, ox).item<float>();
    if (v > obs_threshold) break;
    // add left and right
    for (size_t j=1; j<right_ray.size(); j++) {
      int x = ox+right_ray[j].first;
      int y = oy+right_ray[j].second;
      float v = obs_map.at(x, y).item<float>();
      if (v > obs_threshold) break;
      prob += prob_map.at(x, y).item<float>();
      if (debug) {
        prob_map.set(x, y, torch::tensor(1.0));
      }
    }
    for (size_t j=1; j<left_ray.size(); j++) {
      int x = ox+left_ray[j].first;
      int y = oy+left_ray[j].second;
      float v = obs_map.at(x, y).item<float>();
      if (v > obs_threshold) break;
      prob += prob_map.at(x, y).item<float>();
      if (debug) {
        prob_map.set(x, y, torch::tensor(1.0));
      }
    }
  }
  // Fill in triangle at the end
  for(size_t i=0; i<end.size(); i++) {
    // std::cout << "boo" << left_ray.size() << ", " << right_ray.size() << std::endl;
    int &ox = end[i].first;
    int &oy = end[i].second;
    // add values from center of line
    prob += prob_map.at(ox, oy).item<float>();
    float v = obs_map.at(ox, oy).item<float>();
    if (v > obs_threshold) break;
    if (i >= right_ray.size() || i >= left_ray.size()) {
      continue;
    }
    // add left and right
    for (size_t j=1; j<right_ray.size()-i; j++) {
      int x = ox+right_ray[j].first;
      int y = oy+right_ray[j].second;
      float v = obs_map.at(x, y).item<float>();
      if (v > obs_threshold) break;
      prob += prob_map.at(x, y).item<float>();
      if (debug) {
        prob_map.set(x, y, torch::tensor(1.0));
      }
    }
    for (size_t j=1; j<left_ray.size()-i; j++) {
      int x = ox+left_ray[j].first;
      int y = oy+left_ray[j].second;
      float v = obs_map.at(x, y).item<float>();
      if (v > obs_threshold) break;
      prob += prob_map.at(x, y).item<float>();
      if (debug) {
        prob_map.set(x, y, torch::tensor(1.0));
      }
    }
  }


  // printf("Path prob: %f, %f, %f\n", fov, max_d, prob);
  return prob;
}

float ang_diff(float a1, float a2) {
    float diff = abs(a1 - a2);
    if (diff > 2*M_PI) diff = 2*M_PI - diff;
    return diff;
}

void PMap::add_observation(Pose & pose, torch::Tensor binned_obs, float fx, float cx) {
  const std::vector<std::pair<int, int>> neighbors { {+1, 0}, {-1, 0}, {0, +1}, {0, -1} };
  // binned_obs (N, 2) float (confidence, dist)
  // dist has to be planar camera distance, not spherical
  // TEMP VALUES in M_PI/2
  torch::Tensor rotation = rot(pose.theta-M_PI/2);

  auto origin_angle = [pose](float x, float y) {
    return atan2(y - pose.y, x - pose.x);
  };

  // auto origin_dist = [pose](float x, float y) {
  //   return sqrt(pow(y - pose.y.item<float>(), 2) + pow(x - pose.x.item<float>(), 2));
  // };
  auto origin_dist = [rotation, pose](float x, float y) {
    // This is the z value along the camera's origin, not the euclidean distance
    // to the camera. This aligns with what we are given in the binned obs
    torch::Tensor diff = torch::tensor({x - pose.x, y - pose.y});
    diff = rotation.transpose(0, 1).matmul(diff.reshape({2, 1})).squeeze();
    return diff[1].item<float>();
  };

  auto calc_bound = [rotation](float bound) {
    torch::Tensor lray = rotation.matmul(torch::tensor({bound, 1.0f}).reshape({2, 1})).squeeze();
    return atan2(lray[1].item<float>(), lray[0].item<float>());
  };

  float langle = calc_bound((-binned_obs.size(0)+cx)/fx);
  float rangle = calc_bound((binned_obs.size(0)-cx)/fx);

  auto is_in_bounds = [origin_angle, langle, rangle](float x, float y) {
    float angle = origin_angle(x, y);
    if (langle > rangle) {
      return angle <= langle && angle >= rangle;
    } else {
      return angle <= langle || angle >= rangle;
    }
  };

  // <ray angle, confidence, distance>
  std::vector<AngRay> rays = obs_to_rays(pose, binned_obs, fx, cx, map_res);
  float max_d = -1;
  for (auto ray : rays) {
    max_d = std::max(max_d, ray.dist);
  }
  auto sorted_rays = rays;

  std::sort(sorted_rays.begin(), sorted_rays.end());
  float max_ang_diff = 3*abs(rays.begin()->angle - rays.at(1).angle);
  // <distance from origin, x, y>
  std::vector<Ray> toExplore;
  std::make_heap(toExplore.begin(), toExplore.end());
  toExplore.push_back(Ray{0, (int)ceil(pose.x), (int)ceil(pose.y)});
  toExplore.push_back(Ray{0, (int)floor(pose.x), (int)floor(pose.y)});
  toExplore.push_back(Ray{0, (int)ceil(pose.x), (int)floor(pose.y)});
  toExplore.push_back(Ray{0, (int)floor(pose.x), (int)ceil(pose.y)});

  // std::unordered_set<std::string> visited;
  std::unordered_set<Coord, CoordHash> visited;
  std::vector<Coord> contour;

  while(toExplore.size() > 0) {
    std::pop_heap(toExplore.begin(), toExplore.end());
    Ray cur = toExplore.back();
    toExplore.pop_back();
    int x = cur.x;
    int y = cur.y;
    float curAngle = origin_angle(x, y);
    auto res = std::upper_bound(sorted_rays.begin(), sorted_rays.end(), AngRay{curAngle, 0, 0});
    if (res != sorted_rays.end()) {
      auto lower = res-1;
      res = (ang_diff(curAngle, lower->angle) < ang_diff(curAngle, res->angle)) ? lower : res;
    }
    float diff = ang_diff(curAngle, res->angle);

    if (res != rays.end() && diff < max_ang_diff) {
      AngRay& ray = *res;
      if (ray.dist == 0) {
        ray = rays[0];
      }
      float conf = ray.conf;
      float dist = ray.dist; 
      // Update map probability
      if(origin_dist(x, y) > dist + sqrt(2)) {
        // if(origin_dist(x, y) - dist < sqrt(2)) {
        // }
        contour.push_back(std::make_pair(x, y));
        continue;
      } else if (abs(origin_dist(x, y) - dist) < sqrt(2)) {
        // printf("x: %i, y: %i, Dists: %f, %f, Conf: %f, Before: %f, ", x, y, origin_dist(x, y), dist, conf, prob_map.at(x, y).item<float>());
        update_tile(x, y, conf, dist);
        // printf("After: %f\n", prob_map.at(x, y).item<float>());
      } else {
        update_tile(x, y, 0, dist);
      }
      frontier_map.delete_frontier(x, y);
      known_map.set(x, y, torch::tensor(1));
    } else {
      // contour.push_back(std::make_pair(x, y));
    }
    if (origin_dist(x, y) > max_d + sqrt(2)) {
      contour.push_back(std::make_pair(x, y));
      continue;
    };

    // Check if we need to explore neighbors 
    for (Coord neighbor_rel: NEIGHBORS) {
      Coord neighbor = {neighbor_rel.first+x, neighbor_rel.second+y};
      if (is_in_bounds(neighbor.first, neighbor.second)) { 
        if (visited.find(neighbor) == visited.end()) {
          toExplore.push_back(Ray{origin_dist(neighbor.first, neighbor.second), neighbor.first, neighbor.second});
          std::push_heap(toExplore.begin(), toExplore.end());
          visited.insert(neighbor);
        }
      } else {
        contour.push_back(neighbor);
      }
    }
  }
  prob_map *= 1/prob_map.sum();
  // Update frontier map
  // std::vector<Coord> contours = rays_to_contour(rays, pose);
  frontier_map.add_frontiers(contour, this);
  // printf("Rays\n");
  // for (auto ray : rays) {
  //   printf("%f, %f\n", ray.angle, ray.dist);
  // }
  // printf("Done\n");
  // for (Coord coord : contours) {
  //   printf("%i, %i\n", coord.first, coord.second);
  // }
  // printf("Done\n");
}

void PMap::update_tile(int x, int y, float confidence, float dist)
{
  // TEMP VALUES
  float threshold = 0.5;

  torch::Tensor prob = this->prob_map.at(x, y);
  torch::Tensor tp = this->TP_map.at(x, y);
  torch::Tensor fp = this->FP_map.at(x, y);
  torch::Tensor tn = this->TN_map.at(x, y);
  torch::Tensor fn = this->FN_map.at(x, y);
  torch::Tensor p_t_in_c = this->base_tag_prob_map.at(x, y);
  // p_t_in_c = p
  torch::Tensor prev_denom = (p_t_in_c * torch::exp(tp) * torch::exp(fn) + (1 - p_t_in_c) * torch::exp(fp) * torch::exp(tn)).clone();
  float numer_prod;

  if (confidence > threshold) {
    tp += torch::log(torch::tensor(tpr(dist)));
    fp += torch::log(torch::tensor(fpr(dist)));
    numer_prod = tpr(dist);
  } else {
    tn += torch::log(torch::tensor(tnr(dist)));
    fn += torch::log(torch::tensor(fnr(dist)));
    numer_prod = fnr(dist);
  }
  // numer_prod = confidence;

  // prev_denom = p + (1-p)
  // next_denom = p*0.2 + (1-p)*0.1
  torch::Tensor next_denom = (p_t_in_c * torch::exp(tp) * torch::exp(fn) + (1 - p_t_in_c) * torch::exp(fp) * torch::exp(tn)).clone();
  next_denom = torch::clip(next_denom, std::numeric_limits<float>::lowest(), 1);
  // if (confidence > threshold) {
  //   printf("Yup: %f -> %f\n", prob.item<float>(), (p_t_in_c * numer_prod * prev_denom / next_denom).item<float>());
  //   printf("prev_denom: %f, next_denom: %f, tp: %f, fp: %f, tn: %f, fn: %f, p_t_in_c: %f\n", prev_denom.item<float>(), next_denom.item<float>(), torch::exp(tp).item<float>(), torch::exp(fp).item<float>(), torch::exp(tn).item<float>(), torch::exp(fn).item<float>(), p_t_in_c.item<float>());
  // }
  // next_denom = torch::clip(next_denom, 0.001, 1);
  // this->prob_map.set(x, y, prob * numer_prod * prev_denom / next_denom);
  this->prob_map.set(x, y, p_t_in_c * numer_prod * prev_denom / next_denom);
  // this->prob_map.set(x, y, numer_prod * prev_denom / next_denom);
  // this->prob_map.set(x, y, next_denom);
}

void PMap::add_uniform(float x, float y, float radius) {
  int gx = x, gy = y;
  int s = 2 * int(radius) + 1;
  torch::Tensor circle = torch::zeros({s, s});

  for (int i = 0; i < s; i++) {
    for (int j = 0; j < s; j++) {
      if (std::sqrt(std::pow(i - radius - 1, 2) + std::pow(j - radius - 1, 2)) <= radius) {
        circle[i][j] = 1.0;
      }
    }
  }
  circle /= circle.sum();

  this->prob_map.add_grid(gx - int(radius), gy - int(radius), circle);
  this->base_tag_prob_map.add_grid(gx - int(radius), gy - int(radius), circle);
}

void PMap::add_gaussian(int x, int y, int std, ChunkMap * map)
{
  torch::NoGradGuard guard;
  torch::Tensor kernel = gaussian_kernel(std, std);
  int s = 2 * std + 1;
  map->add_grid(x - std, y - std, kernel.view({s, s}));
}

void PMap::add_obstacle(int x, int y, int std)
{
  add_gaussian(x, y, std, &this->obs_map);
}

void PMap::add_qr(int x, int y, int std)
{
  add_gaussian(x, y, std, &this->prob_map);
}

float PMap::general_path_prob(std::vector<Pose> & path, ChunkMap & tens_map, bool maxpool)
{
  torch::NoGradGuard guard;
  float prob = 0;
  for (size_t i = 1; i < path.size(); i++)
  {
    // torch::Tensor cells = this->get_grid_cells_btw(path[i - 1].position - origin, path[i].position - origin);
    std::vector<Coord> line;
    create_line(line, path[i - 1].x, path[i - 1].y, path[i].x, path[i].y);
    float length = path[i-1].distance(path[i]);

    int x, y;
    float val = 0;
    int n = 0;
    // obs_map.set(int(origin[0].item<float>()), int(origin[1].item<float>()), torch::tensor(0.5));
    // obs_map.set(int(floor(path[i].x.item<float>())), int(floor(path[i].y.item<float>())), torch::tensor(0.5));
    // obs_map.set(int(floor(path[i-1].x.item<float>())), int(floor(path[i-1].y.item<float>())), torch::tensor(0.5));
    for (auto tup : line) {
      std::tie(x, y) = tup;
      // if (x < 0 || y < 0 || x >= tens_map.size(0) || y >= tens_map.size(1)) {
      //   continue;
      // }
      // float v = tens_map[x][y].item<float>();
      float v = tens_map.at(x, y).item<float>();
      if (v == CMAP_NIL) {
        continue;
      }
      // prob is weighted by the length of how much the path cross through the cells
      // obs_map.set(x + int(floor(origin[0].item<float>())), y + int(floor(origin[1].item<float>())), torch::tensor(0.5));
      if (maxpool) {
        val = std::max(val, v);
      } else {
        val += v;
        n += 1;
      }
    }
    if (!maxpool) {
      val *= length/n; 
    }
    prob += val;
  }
  return prob;
}

float PMap::obs_prob(std::vector<Pose> & path)
{
  // torch::Tensor tens_map, origin;
  // std::tie(tens_map, origin) = this->obs_map.stitch_map();
  return general_path_prob(path, this->obs_map, true);
}

// float PMap::path_prob(std::vector<Pose> & path)
// {
//   // torch::Tensor tens_map, origin;
//   // std::tie(tens_map, origin) = this->prob_map.stitch_map();
//   return general_path_prob(path, this->prob_map, false);
// }

torch::Tensor
PMap::test(float radius)
{
  torch::NoGradGuard guard;
  torch::Tensor tens_map, origin;
  std::tie(tens_map, origin) = this->prob_map.stitch_map();
  int r = radius*2;
  long int h = tens_map.size(0);
  long int w = tens_map.size(1);
  torch::Tensor kernel = gaussian_kernel(r, radius);
  torch::Tensor blur = F::conv2d(tens_map.reshape({1, 1, h, w}), kernel, F::Conv2dFuncOptions().padding(r)).reshape({h, w});
  return blur;
}

bool 
PMap::is_free(Coord &c) {
  return (obs_map.at(c.first, c.second) < obs_threshold).item<bool>();
}

bool 
PMap::is_unknown(Coord &c) {
  return (known_map.at(c.first, c.second) == known_map.default_value()).item<bool>();
}

// https://gamedev.stackexchange.com/a/165295
torch::Tensor PMap::get_grid_cells_btw(torch::Tensor p1, torch::Tensor p2)
{
  torch::Tensor x1 = p1[0];
  torch::Tensor y1 = p1[1];
  torch::Tensor x2 = p2[0];
  torch::Tensor y2 = p2[1];
  torch::Tensor dx = x2 - x1;
  torch::Tensor dy = y2 - y1;
  torch::Tensor xs, ys;
  if (dx.item<float>() == 0.0f)
  {
    //will divide by dx later, this will cause err.
    //Catch this case up here
    torch::Tensor step = torch::sign(dy);
    ys = torch::arange(0.0f, (dy + step).item<float>(), step.item<float>());
    xs = x1*torch::ones({ys.size(0)});
  } 
  else
  {
    torch::Tensor m = dy / (dx + 0.0);
    torch::Tensor b = y1 - m * x1;
    torch::Tensor step = 1.0 / (torch::max(torch::abs(dx), torch::abs(dy)));
    xs = torch::arange(x1.item<float>(), x2.item<float>(), (step * torch::sign(x2 - x1)).item<float>());
    ys = xs *m + b;
  }
  torch::Tensor pts = torch::stack({xs, ys}, 1);
  return pts;
}

std::vector<Node>
PMap::generate_frontier_nodes(int pts_per_cluster)
{
  // TODO
  float remaining_prob = 1;
  auto weight_func = [](Coord &c, float percent_unknown) {
    return percent_unknown / (1+0.1*sqrt(c.first*c.first + c.second*c.second));
  };
  return frontier_map.generate_clusters(pts_per_cluster, remaining_prob, weight_func);
}