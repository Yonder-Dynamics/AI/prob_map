import numpy as np
import time
import torch
import random
import math
import yaml
from tqdm import tqdm


import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import colors
import cv2
import util
import math
from matplotlib.collections import LineCollection

from pathlib import Path
import yep
from pmapcpp import Node, Graph, Pose, PMap, RTRRTstar
from observation_sim2021 import ObservationSim, SensorModel


def construct_test_map1(pmap, sensor_model):
    # Construct Pmap
    pmap.add_uniform(0, 0, 90)
    std = 3

    # Quadrant 2 vertical line (-30, 10) --> (-30, 70)
    for y in range(10, 70):
        x = -30
        pmap.add_obstacle(x, y, std)

    # Quadrant 3 horizontal line (-20, -40) --> (-80, -40)
    for x in range(-80, -20):
        y = -40
        pmap.add_obstacle(x, y, std)

    # Quadrant 4 diagonal line (20, -70) --> (70, -20)
    for x in range(20, 70):
        y = -(70 - x + 20)
        pmap.add_obstacle(x, y, std)

    # Quadrant 1 right angle (30, 55) --> (55, 55), (55, 30) --> (55, 55)
    for i in range(30, 55):
        const = 55
        pmap.add_obstacle(i, const, std)
        pmap.add_obstacle(const, i, std)

    # assign tags
    rtags = torch.tensor([[65, 65], [55, 65]])
    ftags = torch.tensor(
        [[-45, 40], [-60, -55], [52, -52], [0, 90], [0, -90], [90, 0], [-90, 0]]
    )

    # Simulate observation
    return ObservationSim(sensor_model, real_tags=rtags, fake_tags=ftags)


def construct_test_map2(pmap, sensor_model):
    # Construct Pmap
    pmap.add_uniform(0, 0, 90)
    std = 3

    # Quadrant 1 -> 4 outer vertical line (25, 25) --> (25, -50)
    for y in range(-50, 25):
        x = 25
        pmap.add_obstacle(x, y, std)

    # Quadrant 4 -> 3 outer horizontal line (25, -50) --> (-50, -50)
    for x in range(-50, 25):
        y = -50
        pmap.add_obstacle(x, y, std)

    # Quadrant 3 -> 2 outer vertical line (-50, -50) --> (-50, 50)
    for y in range(-50, 50):
        x = -50
        pmap.add_obstacle(x, y, std)

    # Quadrant 2 -> 1 outer horizontal line (-50, 50) --> (25, 50)
    for x in range(-50, 25):
        y = 50
        pmap.add_obstacle(x, y, std)

    # y-axis vertical line (0, 50) --> (0, 25)
    for y in range(25, 50):
        x = 0
        pmap.add_obstacle(x, y, std)

    # y-axis -> Q2 inner horizontal lines (0, 25) --> (-25, 25), (0, -25) --> (-25, -25)
    for x in range(-25, 0):
        y_mag = 25
        pmap.add_obstacle(x, y_mag, std)
        pmap.add_obstacle(x, -y_mag, std)

    # Quadrant 2 -> 3 inner vertical line (-25, 25) --> (-25, -25)
    for y in range(-25, 25):
        x = -25
        pmap.add_obstacle(x, y, std)

    # assign tags
    rtags = torch.tensor([[-10, 32], [-10, 42]])
    ftags = torch.tensor(
        [[50, 50], [60, 10], [10, -40], [90, -90], [-90, -90], [-90, 90]]
    )

    # Simulate observation
    return ObservationSim(sensor_model, real_tags=rtags, fake_tags=ftags)


def construct_test_map_easy(pmap, sensor_model):
    # Construct Pmap
    pmap.add_uniform(0, 0, 90)
    std = 3

    # Walls
    for y in range(-50, 25):
        pmap.add_obstacle(-25, y, std)
        pmap.add_obstacle(25, y, std)
    for x in range(-25, 25):
        pmap.add_obstacle(x, 25, std)

    rtags = torch.tensor([[10, -40], [-10, -40]])
    ftags = torch.tensor(
        [[50, 50], [60, 10], [90, -90], [-90, -90], [-90, 90]])

    # Simulate observation
    return ObservationSim(sensor_model, real_tags=rtags, fake_tags=ftags)


# function isnt used
def disp_obs(pose, obs, sensor_model, threshold=0.5):
    xs = []
    ys = []
    us = []
    vs = []
    cs = []
    #  norm = colors.LogNorm(vmin=float(obs[:, 0].min()), vmax=float(obs[:, 0].max()))
    #  cmap = plt.get_cmap('viridis')
    for i in range(sensor_model.res):
        if threshold is not None and obs[i, 0] < threshold:
            continue
        diff = torch.tensor([(i - sensor_model.cx) / sensor_model.fx, 1])
        ray = (util.rot(pose.orientation - math.pi / 2)
               @ diff.reshape(2, 1)).reshape(2)
        ray = ray * obs[i, 1]
        xs.append(pose.position[0])
        ys.append(pose.position[1])
        us.append(ray[0])
        vs.append(ray[1])
        cs.append(obs[i, 0])
    plt.quiver(xs, ys, us, vs, cs, linewidth=0.5, scale=1, units="x", headwidth=0.5, headlength=0.5, c='blue')


KEY_ANIMATED = "animated"
KEY_ALLOWED_TIME = "allowed_time"
KEY_RES = "resolution"
KEY_PMAP_CHUNK_SIZE = "pmap_chunk_size"
KEY_RRT_CHUNK_SIZE = "rrt_chunk_size"
KEY_OBS_THRESHOLD = "obs_threshold"
KEY_RRT_RADIUS = "rrt_radius"
KEY_RRT_EPSILON = "epsilon"
KEY_SAMPLE_RADIUS = "sample_radius"
KEY_RRT_MAX_D = "max_d"
KEY_FOV = "fov"
KEY_RRT_N_INIT_NODES = "n_init_nodes"
KEY_RRT_DISCOUNT_FACTOR = "discount_factor"
KEY_PRINT_GRAPH = "print_graph"


class Driver:
    def __init__(self, config, root_pos):
        """
        config - file name
        """
        with open(config, "r") as config_file:
            self._config = yaml.load(config_file, Loader=yaml.FullLoader)
        self._root_pos = root_pos

        if self._config[KEY_ANIMATED]:
            Path("animation").mkdir(exist_ok=True)

        # Generate pmap
        obs_threshold = self._config[KEY_OBS_THRESHOLD]
        self.pmap = PMap(
            obs_threshold, res=self._config[KEY_RES], chunk_size=self._config[KEY_PMAP_CHUNK_SIZE]
        )

        self.sensor_model = SensorModel(25, 26, self._config[KEY_RRT_MAX_D], 51, [3, 8], 0.2, 0.01)

        self.obs_sim = construct_test_map2(self.pmap, self.sensor_model)

        # Spin a circle at the origin
        # for angi in torch.linspace(0, 2 * math.pi, 5)[0:]:
        #     pose = Pose(
        #         self._root_pos.position[0],
        #         self._root_pos.position[1],
        #         angi + math.pi / 2,
        #     )
        #     observ = self.obs_sim.gen_observation(pose, self.pmap)
        #     self.pmap.add_observation(
        #         pose, observ, self.obs_sim.fx, self.obs_sim.cx
        #     )
        #     # driver.obs_received(observ, obs_sim.fx, obs_sim.cx)
        print(f"Sensor model fov: {self.sensor_model.fov}")

        self._rrt = RTRRTstar(
            self._root_pos,
            self.pmap,
            self._config[KEY_RRT_CHUNK_SIZE],
            self._config[KEY_ALLOWED_TIME],
            self._config[KEY_RRT_RADIUS],
            self._config[KEY_RRT_EPSILON],
            self._config[KEY_SAMPLE_RADIUS],
            self.sensor_model.max_dist,
            self.sensor_model.fov,
        )

        (prob_min_x, prob_min_y), (
            prob_max_x,
            prob_max_y,
        ) = self.pmap.prob_map.get_bounds()
        (obs_min_x, obs_min_y), (obs_max_x,
                                 obs_max_y) = self.pmap.obs_map.get_bounds()

        # absolute extreme coordinates
        self._min_x = min(prob_min_x, obs_min_x)
        self._min_y = min(prob_min_y, obs_min_y)
        self._max_x = max(prob_max_x, obs_max_x)
        self._max_y = max(prob_max_y, obs_max_y)

        # convert to chunk coordinates
        self._min_cx = self._min_x // self.pmap.prob_map.chunk_size
        self._min_cy = self._min_y // self.pmap.prob_map.chunk_size
        self._max_cx = self._max_x // self.pmap.prob_map.chunk_size
        self._max_cy = self._max_y // self.pmap.prob_map.chunk_size

        # COLORS
        self._norm = colors.LogNorm(vmin=1e-7, vmax=1e-3)
        self._it = 0

        self._main_path = [self._root_pos]
        print("Initializing")
        for i in tqdm(range(self._config[KEY_RRT_N_INIT_NODES])):
            self._rrt.next_iter(self._root_pos)

        for it in range(100):
            self._rrt.update_frontier()
            self.waypoint_reached()

            observ = self.obs_sim.gen_observation(self._root_pos, self.pmap)
            self.obs_received(observ)

    def _get_child_val(self, child):
        return child.recursive_value(self._config[KEY_RRT_DISCOUNT_FACTOR])

    def waypoint_reached(self):
        start_time = time.time()
        self._rrt.paint_graph()
        print(f"Took {time.time()-start_time} to paint graph")
        start_time = time.time()
        for i in range(10):
            self._rrt.next_iter(self._root_pos)
        print(f"Took {time.time()-start_time} to run RRT")

        next_node = max(self._rrt.graph.root.children, key=self._get_child_val)
        print(f'Chose path with value {self._get_child_val(next_node)}')
        next_node.pose.orientation = math.atan2(
            next_node.pose.y - self._root_pos.y, next_node.pose.x - self._root_pos.x
        )
        traveled_path = [next_node.pose]
        self._main_path += traveled_path

        # Update root to be at the end of path
        self._root_pos = traveled_path[-1]

    def obs_received(self, obs):
        start_time = time.time()
        self.pmap.add_observation(
            self._root_pos, obs, self.sensor_model.fx, self.sensor_model.cx
        )
        print(f"Add obs: {time.time()-start_time}")

        if self._config[KEY_PRINT_GRAPH]:
            # Gather nodes to plot
            disp, origin = self.pmap.prob_map.stitch_map(
                # disp, origin = self.pmap.known_map.stitch_map(
                self._min_cx, self._max_cx, self._min_cy, self._max_cy
            )
            obs_disp, _ = self.pmap.obs_map.stitch_map(
                self._min_cx, self._max_cx, self._min_cy, self._max_cy
            )
            frontier_disp, _ = self.pmap.frontier_map.frontier_inds.stitch_map(
                self._min_cx, self._max_cx, self._min_cy, self._max_cy
            )
            plt.clf()
            plt.cla()
            plt.close()
            fig, ax = plt.subplots(1, 1)

            ax.imshow(
                disp.T,
                extent=(self._min_x, self._max_x, self._min_y, self._max_y),
                origin="lower",
                cmap="hot",
                # norm=self._norm,
            )
            ax.imshow(
                obs_disp.T > self.pmap.obs_threshold,
                extent=(self._min_x, self._max_x, self._min_y, self._max_y),
                origin="lower",
                alpha=0.3,
                cmap="Blues",
            )


            # ax.imshow(
            #     frontier_disp.T >= 0,
            #     extent=(self._min_x, self._max_x, self._min_y, self._max_y),
            #     origin="lower",
            #     cmap="Greys",
            #     alpha=0.3,
            # )

            start_time = time.time()
            # Draw graph
            edges = []
            values = []
            frontier_nodes = []
            for i in range(len(self._rrt.graph)):
                # pass
                node = self._rrt.graph.get_node(i)
                if node.is_frontier:
                    frontier_nodes.append(node.pose.position.numpy())
                for child in node.children:
                    edges.append(
                        [node.pose.position.numpy(), child.pose.position.numpy()]
                    )
                    values.append(self._get_child_val(child))
            print(len(edges))
            cmap = plt.get_cmap("viridis")
            values = [0 if np.isnan(v) else v for v in values]
            norm2 = plt.Normalize(vmin=min(values), vmax=max(values))
            colors = [cmap(norm2(v)) for v in values]

            path_pos = [pose.position.numpy() for pose in self._main_path]
            path_segs = list(zip(path_pos[:-1], path_pos[1:]))

            line_segments = LineCollection(
                edges, linewidths=(1.5), colors=colors, linestyle="solid"
            )
            ax.add_collection(line_segments)

            line_segments = LineCollection(
                path_segs, linewidths=(3.0), colors="green", linestyle="solid"
            )
            ax.add_collection(line_segments)
            ax.scatter(self._root_pos.x, self._root_pos.y,
                       c="red", s=10, zorder=100)
            frontier_nodes = np.array(frontier_nodes)
            if len(frontier_nodes) != 0:
                ax.scatter(frontier_nodes[..., 0], frontier_nodes[..., 1], c="orange", s=10)

            self.obs_sim.graph()
            # disp_obs(self._root_pos, obs, self.sensor_model)

            start_time = time.time()
            plt.savefig(f"animation/{self._it:04d}.png", dpi=400)
            print(f"Save graph: {time.time()-start_time}")
            self._it += 1

        #  if graph.end_condition(edge_index, 0.7):
        #      break


if __name__ == "__main__":
    yep.start('iter1.prof')
    root_pos = Pose(torch.tensor([0.0, 0.0]), torch.tensor([0.0]))
    driver = Driver("config/test_config.yaml", root_pos)

    # Generate pmap

    # circle around last segment
    tag_segment = driver._main_path[-1]
    #  if print_graph:
    #      plt.imshow(disp.T, extent=(min_x, max_x, min_y, max_y), origin='lower')
    #      x, y = zip(*[pose.position for pose in main_path])
    #      plt.plot(x, y, c="black", linewidth=3.0)
    #      plt.plot(x, y, c="green", linewidth=2.0)
    #      plt.show()

    yep.stop()
