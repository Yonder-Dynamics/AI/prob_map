import numpy as np

# This module generates paths to search an isoregion
def generate_path(origin, isoregion, robot_params):
    # TODO, probably use some off the shelf algorithm
    # Temporary stuff for testing
    # Find center
    lx = np.arange(isoregion.shape[0])
    ly = np.arange(isoregion.shape[1])
    gx, gy = np.meshgrid(lx, ly)

    norm_region = isoregion/np.sum(isoregion)

    endpoint = (norm_region*gx, norm_region*gy)
    path = [origin, endpoint]
    travel_cost = robot_params.speed*np.sqrt(
            (endpoint[0]-origin[0])**2 + (endpoint[0]-origin[0])**2)
    explore_cost = robot_params.search_speed*np.sum(isoregion > 0)
    return path, travel_cost + explore_cost
