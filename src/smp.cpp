#include "pmap/smp.h"
#include "pmap/graph.h"
#include "pmap/pmap.h"
#include <chrono>
#include <utility>

using namespace std::chrono;

//--------------------------------------------------------------Robot.h
//-------------------------------------------------------------General
#define max_neighbours 50
#define alpha 0.1
#define beta 1.4

void init_smp_module(py::module &m) {
  py::class_<RTRRTstar>(m, "RTRRTstar")
      .def(py::init<Pose, PMap &, int, float, float, float, float, float, float>())
      .def("next_iter", &RTRRTstar::next_iter)
      .def("update_frontier", &RTRRTstar::update_frontier)
      .def("paint_graph", &RTRRTstar::paint_graph)
      .def_readwrite("graph", &RTRRTstar::graph);
}

inline float rand_range(float minv, float maxv) {
  return static_cast<float>(rand()) / static_cast<float>(RAND_MAX) *
             (maxv - minv) +
         minv;
}

torch::Tensor rand_circle(float radius, torch::Tensor origin) {
  float x = rand_range(-radius, radius);
  float y = rand_range(-radius, radius);
  if (sqrt(x * x + y * y) < radius) {
    return torch::tensor({x, y}) + origin;
  } else {
    return rand_circle(radius, origin);
  }
}

Pose irrt_sample(Pose &start, Pose &goal, float c_max, float margin) {
  // Informed RRT
  float c_min = start.distance(goal);

  torch::Tensor x_centre = (start.position() + goal.position()) / 2;
  torch::Tensor dir = goal.position() - start.position();
  dir /= torch::norm(dir);

  float angle = torch::atan2(dir[1], dir[0]).item<float>();
  float r1 = c_max / 2;
  float r2 = std::sqrt(std::pow(c_max, 2) - std::pow(c_min, 2)) / 2;

  float x = rand_range(-1, 1);
  float y = rand_range(-1, 1);
  float theta = rand_range(-M_PI, M_PI);

  float x2 = x * r1 * std::cos(angle) + y * r2 * std::sin(angle);
  float y2 = -x * r1 * std::sin(angle) + y * r2 * std::cos(angle);

  Pose outpose(x2, y2, theta);
  outpose.x += x_centre[0].item<float>();
  outpose.y += x_centre[1].item<float>();

  // Unconnected node
  return outpose;
}

float RTRRTstar::elapsed_time() {
  auto ms = system_clock::now() - last_recorded_time;
  float seconds = duration_cast<milliseconds>(ms).count() / 1000.0;
  return seconds;
}

Pose RTRRTstar::uniform_sample() {
  Pose outpose(rand_circle(sample_radius, graph.root()->pose.position()),
               torch::tensor(0.0f));
  return outpose;
}

Pose RTRRTstar::sample() {
  float rand_num = rand_range(0, 1);

  if (rand_num > 1 - alpha && target != NULL) {
    float x = rand_range(graph.root()->pose.x, target->pose.x);
    float y = rand_range(graph.root()->pose.y, target->pose.y);
    return Pose(x, y, 0.0f);
  }
  // TODO What should the goal be?
  // else if (rand_num >= (1 - alpha) / beta && goal_found)
  // {
  //   return irrt_sample(graph.root(), goal, target->recursive_cost()); //Still
  //   using informed RRT*?
  // }
  else {
    return uniform_sample();
  }
}

bool RTRRTstar::is_sample_clear(Pose &p) {
  Coord c = p.coord();
  return map->is_free(c);
}

bool RTRRTstar::is_path_clear(Pose &p1, Pose &p2) {
  return map->check_path_clear({p1, p2});
}

// TODO: Add the node to the Grid based/KD-Tree Data structure
size_t
RTRRTstar::add_node(Pose p, Node *closest,
                    std::vector<std::pair<unsigned int, float>> neighbors) {
  // Neighbors are the neighbors of pose p
  float c_min = closest->recursive_cost() + p.distance(closest->pose);
  Node *xmin = closest;
  // IDK what this is for, but removing it makes no difference - Alex
  // for (auto it : neighbors) {
  //   Node *sel = graph.get_node(it.first);
  //   float c_new = sel->recursive_cost() + p.distance(sel->pose);
  //   if (c_new < c_min && check_collision(p, sel->pose)) {
  //     c_min = c_new;
  //     xmin = sel;
  //   }
  // }

  Node xnew(p, xmin);
  size_t j = graph.add_node(xnew);
  Node *loc = graph.get_node(j);
  xmin->children.push_back(loc);
  this->rewire_rand.push_front(loc);
  return j;
}

void RTRRTstar::next_iter(Pose &robot_pose) // What is Robot??
{
  last_recorded_time = system_clock::now();
  // change root node
  float margin = 2;
  if (robot_pose.distance(graph.root()->pose) > margin) {
    size_t i;
    float dist;
    std::tie(i, dist) = graph.get_closest_node(
        robot_pose); // Using all the nodes for the time being
    Node *closest = graph.get_node(i);
    if (closest->pose.distance(robot_pose) < margin) {
      // changes root to existing node
      printf("Existing\n");
      change_root(i);
    } else {
      printf("Create new\n");
      std::vector<std::pair<unsigned int, float>> neighbors =
          graph.get_neighbors(robot_pose, rrtstarradius);
      size_t j = add_node(robot_pose, closest, neighbors);
      change_root(j);
    }
  }
  for(int i=0; i<10; i++) {

    Pose pose = sample();
    for (size_t i=0; i<20; i++) {
      Coord c = std::make_pair((int)pose.x, (int)pose.y);
      if (is_sample_clear(pose)){// && !map->is_unknown(c)) {
        break;
      }
      pose = sample();
    }

    if (expand_and_rewire(pose)) {
      break;
    }
  }

  for (auto it : queued_goals) {
    expand_and_rewire(it.pose);
  }
}

void RTRRTstar::change_root(size_t node_ind) {
  // Node *old_root = graph.root();
  graph.change_root(node_ind);
  // Node *new_root = graph.root();

  // Node *next_point = graph.get_node(node_ind);

  // old_root->cost = old_root->pose.distance(new_root->pose);

  visited_set.clear();
  already_rewired.clear();
  rewire_root.clear();
}

void
RTRRTstar::update_frontier() {
  std::vector<Node> goals = map->generate_frontier_nodes(30);
  // First, reset existing goals
  for (auto goal : added_goals) {
    goal->is_frontier = false;
    goal->base_value = 0.0f;
  }
  added_goals.clear();
  queued_goals.clear();
  for (auto goal : goals) {
    queued_goals.push_back(goal);
  }
}

bool
RTRRTstar::try_connect_goal(size_t node_ind)
{
  float margin = 2;
  Node *node = graph.get_node(node_ind);

  float min_dist = std::numeric_limits<float>::max();
  Node *min_goal = NULL;
  bool is_added = false;

  for (auto it : added_goals) {
    float dist = it->pose.distance(node->pose);
    if (dist < min_dist) {
      min_dist = dist;
      min_goal = it;
      is_added = true;
    }
  }

  auto min_goal_it=queued_goals.begin();
  for (auto it=queued_goals.begin(); it!=queued_goals.end(); it++) {
    float dist = it->pose.distance(node->pose);
    if (dist < min_dist) {
      min_dist = dist;
      min_goal = &(*it);
      min_goal_it = it;
      is_added = false;
    }
  }

  if (min_dist < margin) {
    printf("min_dist: %f, %f, %f, %f, %f\n", min_dist, node->pose.x, node->pose.y,
          min_goal->pose.x, min_goal->pose.y);
    if (!is_added) {
      node->is_frontier = true;
      node->base_value = min_goal->base_value;
      added_goals.push_back(node);
      this->rewire_rand.push_front(node);

      queued_goals.erase(min_goal_it);
    }
    return true;
  }
  return false;
}

bool RTRRTstar::expand_and_rewire(Pose &pose) {
  std::vector<std::pair<unsigned int, float>> neighbors =
      graph.get_neighbors(pose, rrtstarradius);
  float d;
  int i;
  std::tie(i, d) = graph.get_closest_node(pose);
  Node *v = graph.get_node(i);
  float dist = pose.distance(v->pose);

  if (dist > epsilon) {
    pose.x = v->pose.x + (pose.x - v->pose.x) * epsilon / dist;
    pose.y = v->pose.y + (pose.y - v->pose.y) * epsilon / dist;
  }

  // Done selecting the pose we are trying to connect

  if (is_path_clear(pose, v->pose)) {
    // Enforce graph sparsity
    if (neighbors.size() < max_neighbours && pose.distance(v->pose) > 2)
    {
      size_t node_ind = this->add_node(pose, v, neighbors);
      try_connect_goal(node_ind);
    } else {
      // This is done in add node anyways
      this->rewire_rand.push_front(v);
    }
    rewire_random_node();
    rewire_from_root();
    return true;
  } else {
    return false;
  }
}

void RTRRTstar::paint_graph() {
  for (auto it : graph.nodes) {
    Node &n = it.second;
    for (auto c : n.children) {
      if (!c->is_frontier) {
        c->base_value = map->path_prob(n.pose, c->pose, max_d, fov);
      }
    }
  }
}

// Uses nodes to pass to closest neighbours
void RTRRTstar::rewire_random_node() {
  while (!rewire_rand.empty() && elapsed_time() < 0.5 * allowed_time_rewiring) {
    Node *Xr = rewire_rand.front();
    rewire_rand.pop_front();

    std::vector<std::pair<unsigned int, float>> neighbors =
        graph.get_neighbors(Xr->pose, rrtstarradius);

    float cost = Xr->recursive_cost();
    for (auto it : neighbors) {
      Node *sel = graph.get_node(it.first);
      if (!is_path_clear(Xr->pose, sel->pose))
        continue;

      float old_cost = sel->recursive_cost();
      float new_cost = cost + Xr->pose.distance(sel->pose);
      if (new_cost < old_cost) {
        sel->parent->children.remove(sel);
        sel->parent = Xr;
        // sel->cost = new_cost;
        Xr->children.push_back(sel);
        rewire_rand.push_back(sel);
      }
    }
  }
}

void RTRRTstar::rewire_from_root() {
  // graph.print();

  if (rewire_root.empty()) {
    rewire_root.push_back(graph.root());
  }

  while (!rewire_root.empty() && elapsed_time() < allowed_time_rewiring) {
    Node *Xs = rewire_root.front();
    rewire_root.pop_front();
    std::vector<std::pair<unsigned int, float>> neighbors =
        graph.get_neighbors(Xs->pose, rrtstarradius);

    for (auto it : neighbors) {
      Node *sel = graph.get_node(it.first);
      if (!is_path_clear(Xs->pose, sel->pose) || Xs->parent == sel)
        continue;
      float old_cost = sel->recursive_cost();
      float new_cost = Xs->recursive_cost() + Xs->pose.distance(sel->pose);
      if (new_cost < old_cost) {
        sel->parent->children.remove(sel);
        sel->parent = Xs;
        // sel->cost = new_cost;
        Xs->children.push_back(sel);
      }
      bool found = std::find(already_rewired.begin(), already_rewired.end(),
                             sel) != already_rewired.end();
      if (!found) {
        rewire_root.push_back(sel);
        already_rewired.push_back(sel);
      }
    }
  }
}