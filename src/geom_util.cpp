#include "pmap/geom_util.h"

void init_geom_module(py::module &m) {
  py::class_<Pose>(m, "Pose")
    .def(py::init<torch::Tensor, torch::Tensor>())
    .def(py::init<float, float, float>())
    .def("clone", &Pose::clone)
    .def_property_readonly("position", &Pose::position)
    .def_readwrite("y", &Pose::y)
    .def_readwrite("x", &Pose::x)
    .def_readwrite("orientation", &Pose::theta);
}

torch::Tensor rot(float angi)
{
  return torch::tensor({cos(angi), -sin(angi), sin(angi), cos(angi)}).view({2, 2});
}

torch::Tensor rotX(float angi)
{
  float data[] = 
    {1, 0, 0,
     0, cos(angi), -sin(angi),
     0, sin(angi), cos(angi)};
  return torch::from_blob(data, {3, 3});
}

torch::Tensor rotY(float angi)
{
  float data[] = 
    {cos(angi), 0, sin(angi),
     0, 1, 0,
     -sin(angi), 0, cos(angi)};
  return torch::from_blob(data, {3, 3});
}
torch::Tensor rotZ(float angi)
{
  float data[] = 
    {cos(angi), -sin(angi), 0,
     sin(angi), cos(angi), 0,
     0, 0, 1};
  return torch::from_blob(data, {3, 3});
}

float dist(Coord &a, Coord &b) {
  return sqrt((a.first-b.first)*(a.first-b.first) + (a.second-b.second)*(a.second-b.second));
}

void
create_line(std::vector<Coord> &out, float x1, float y1, float x2, float y2)
{
  // Bresenham's line algorithm
  const bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
  if(steep)
  {
    std::swap(x1, y1);
    std::swap(x2, y2);
  }
 
  if(x1 > x2)
  {
    std::swap(x1, x2);
    std::swap(y1, y2);
  }
 
  const float dx = x2 - x1;
  const float dy = fabs(y2 - y1);
 
  float error = dx / 2.0f;
  const int ystep = (y1 < y2) ? 1 : -1;
  int y = (int)y1;
 
  const int maxX = (int)x2;
 
  for(int x=(int)x1; x<=maxX; x++)
  {
    // printf("x: %i, %f, %f, %f, %f, %zu\n", x, x1, y1, x2, y2, out.size());
    if(steep)
    {
        out.push_back(std::pair<int, int>(y, x));
    }
    else
    {
        out.push_back(std::pair<int, int>(x, y));
    }
 
    error -= dy;
    if(error < 0)
    {
        y += ystep;
        error += dx;
    }
  }
}

std::vector<AngRay>
obs_to_rays(Pose & pose, torch::Tensor &binned_obs, float fx, float cx, 
            float map_res)
{

  auto origin_angle = [pose](float x, float y) {
    return atan2(y - pose.y, x - pose.x);
  };

  torch::Tensor rotation = rot(pose.theta-M_PI/2);

  // <ray angle, confidence, distance>
  std::vector<AngRay> rays;
  // For some resolution, create the rays for each observation
  for (int i=0; i<binned_obs.size(0); i++)
  {
    // Raycast from the pose outward according to the camera fov
    float conf = binned_obs[i][0].item<float>();
    float dist = binned_obs[i][1].item<float>();
    float data[] = {
      (i - cx)/fx*dist, dist
    };
    torch::Tensor ray = rotation.matmul(torch::from_blob(data, {2, 1})).squeeze();
    // int x = (ray[0] * dist + pose.x).item<float>() / map_res + 0.5;
    // int y = (ray[1] * dist + pose.y).item<float>() / map_res + 0.5;
    // printf("Ray %i: %i, %i, %f, %f\n", i, x, y, conf, dist);
    float x = ray[0].item<float>();
    float y = ray[1].item<float>();
    float ang = atan2(y, x);
    float euclid_dist = sqrt(x*x + y*y);

    rays.push_back(AngRay{ang, conf, dist, euclid_dist});
  }

  return rays;
}

std::vector<Coord>
rays_to_contour(std::vector<AngRay> &rays, Pose &pose) {
    // Rays are assumed to be in order of increasing angle
    std::vector<Coord> contour;
    int x1, y1, x2, y2, lx, ly, fx, fy;
    int ox = pose.x;
    int oy = pose.y;
    // We first add the line leading along the first ray
    std::tie(fx, fy) = rays.begin()->ray_tip(ox, oy, 0.00, 2*sqrt(2));
    std::tie(lx, ly) = rays[rays.size()-1].ray_tip(ox, oy, 0.00, 2*sqrt(2));
    bool complete = (abs(fx - lx) <= 1 && abs(fy - ly) <= 1);
    if (!complete) {
      create_line(contour, ox, oy, fx, fy);
    }
    // then span the tips of each subsequent ray
    for (size_t i=1; i<rays.size(); i++) {
        std::tie(x1, y1) = rays[i-1].ray_tip(ox, oy, 0, 2*sqrt(2));
        std::tie(x2, y2) = rays[i].ray_tip(ox, oy, 0, 2*sqrt(2));
        if (x1 == x2 && y1 == y2) {
          contour.push_back(std::pair<int, int>(x1, y1));
        } else {
          create_line(contour, x1, y1, x2, y2);
        }
    }

    // then add the line leading along the last ray
    if (!complete) {
      create_line(contour, lx, ly, ox, oy);
    }
    return contour;
}
