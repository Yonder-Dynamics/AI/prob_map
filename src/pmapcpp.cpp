#include <torch/extension.h>

#include <vector>
#include <iostream>
#include <fstream>
// #include <cuda.h>
#include "pmap/chunkmap.h"
#include "pmap/graph.h"
#include "pmap/pmap.h"
#include "pmap/smp.h"
#include "pmap/geom_util.h"
#include "pmap/frontier_map.h"

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
  init_graph_module(m);
  init_chunkmap_module(m);
  init_pmap_module(m);
  init_smp_module(m);
  init_geom_module(m);
  init_frontier_module(m);
}
