#include <torch/extension.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include "pmap/pmap.h"
#define LOG() printf("%s: %i\n", __FILE__, __LINE__)
#include "pmap/cuda_raycaster.h"
#include "../src/pmap_cuda.cu"

#include <vector>

using glm::vec3;


std::vector<torch::Tensor>
cuda_reproject_score_maps(
    std::string octomap_path,
    std::vector<torch::Tensor> & score_maps,
    std::vector<torch::Tensor> & pose_tensors,
    std::vector<float> & calib)
{
  torch::Device cpu("cpu");
  torch::Device gpu("cuda");
  const int batch_size = score_maps.size();
  const int im_rows = score_maps[0].size(0);
  const int im_cols = score_maps[0].size(1);

  CameraConfig cfg {
    .im_rows = im_rows, .im_cols=im_cols,
    .fx = calib[0], .fy = calib[1], .cx = calib[2], .cy = calib[3],
  };
  octree::Octree tree (octomap_path);
  octree::CudaRaycaster caster (tree, cfg);

  int tx = 8;
  int ty = 8;
  dim3 blocks (im_cols/tx, im_rows/ty);
  dim3 threads (tx, ty);

  std::vector<torch::Tensor> targets(2*batch_size);

  // vec3 tree_origin {tree_origin_v[0], tree_origin_v[1], tree_origin_v[2]};

  for(int query_index=0; query_index<batch_size; query_index++) {
    torch::Tensor score_map = score_maps[query_index];
    // torch::Tensor pose = pose_tensors[query_index].to(gpu);
    for (int i=0; i<16; i++) {
      cfg.transform_matrix[i] = pose_tensors[query_index].data_ptr<float>()[i];
    }
    caster.projectScore(cfg, score_map.data_ptr<float>());
  }
  // The format of this is a tensor for each query image, with num x [x y w h]
  // Stores the pos counts x batch_size, then the total_counts x batch_size
  for(int query_index=0; query_index<batch_size; query_index++) {
    auto options = torch::TensorOptions().dtype(torch::kFloat32).device(cpu);
    torch::Tensor score = torch::zeros({im_rows, im_cols}, options);
    torch::Tensor total_counts = torch::zeros({im_rows, im_cols}, options);
    for (int i=0; i<16; i++) {
      cfg.transform_matrix[i] = pose_tensors[query_index].data_ptr<float>()[i];
    }
    caster.renderScoreCount(cfg, score.data_ptr<float>(), total_counts.data_ptr<float>());
    targets[query_index] = score;
    targets[batch_size + query_index] = total_counts;
  }
  return targets;
}
