#include <vector>
#include <string>

#include "pmap/utils.h"

std::vector<std::string> split(const std::string& str, const std::string& delim)
{
  // https://stackoverflow.com/questions/14265581/parse-split-a-std::string-in-c-using-std::string-delimiter-standard-c
  std::vector<std::string> tokens;
  size_t prev = 0, pos = 0;
  do
  {
    pos = str.find(delim, prev);
    if (pos == std::string::npos) pos = str.length();
    std::string token = str.substr(prev, pos-prev);
    if (!token.empty()) tokens.push_back(token);
    prev = pos + delim.length();
  }
  while (pos < str.length() && prev < str.length());
  return tokens;
}

torch::Tensor gaussian_kernel(int radius, float sigma)
{
  torch::Tensor kernel = torch::zeros({1, 1, 2*radius+1, 2*radius+1});
  for (int x=-radius; x<radius+1; x++) {
    for (int y=-radius; y<radius+1; y++) {
      kernel[0][0][x+radius][y+radius] = 1.0f/(2*M_PI*sigma) * std::exp(-(x*x + y*y) / 2 / sigma);
    }
  }
  return kernel;
}

// Code by Wolfgang Brehm on StackOverflow
uint32_t distribute(const uint32_t& n){
  uint32_t p = 0x55555555ul; // pattern of alternating 0 and 1
  uint32_t c = 3423571495ul; // random uneven integer constant; 
  return c*xorshift(p*xorshift(n,16),16);
}

uint64_t uhash(const uint64_t& n){
  uint64_t p = 0x5555555555555555;     // pattern of alternating 0 and 1
  uint64_t c = 17316035218449499591ull;// random uneven integer constant; 
  return c*xorshift(p*xorshift(n,32),32);
}
// END
