import numpy as np

class Pose:
    __slots__ = ["position", "orientation"]
    def __init__(self, position, orientation):
        self.position = position
        self.orientation = orientation


class Edge:
    __slots__ = ['from_i', 'to_i', 'path', 'base_value', 'future_value']
    def __init__(self, from_i, to_i, path, value):
        self.from_i = from_i
        self.to_i = to_i
        self.path = path
        self.base_value = value # probability
        self.future_value = 0

    @property
    def length(self):
        l = 0
        for prev, curr in zip(self.path[:-1], self.path[1:]):
            length = np.sqrt((curr.position[0]-prev.position[0])**2 + (curr.position[1]-prev.position[1])**2)
            l += length
        return l


class Node:
    __slots__ = ['value', 'pose', 'dist', 'depth', 'edge_indices', 'parent', 'children']
    def __init__(self, value, pose, depth, parent):
        self.value = value
        self.pose = pose
        self.dist = 0
        self.depth = depth
        self.edge_indices = []
        self.parent = parent
        self.children = []


class Graph:
    __slots__ = ['edges', 'nodes']
    def __init__(self):
        self.edges = []
        self.nodes = []

    # Then, we need to traverse inward
    def update_future_values(self):
        def dfs_add(curr):
            node = self.nodes[curr]
            if len(node.children) == 0:
                return 0
            # Otherwise iterate through the children
            for e_i in node.edge_indices:
                edge = self.edges[e_i]
                child = self.nodes[edge.to_i]
                child.value = edge.base_value + \
                    gamma**edge.length*dfs_add(edge.to_i)
            return max((self.nodes[child].value for child in node.children))
        dfs_add(0)

    def extract_best_edge(self):
        root = self.nodes[0]
        best_i = max(root.children, key=lambda i: self.nodes[i].value)
        best = self.nodes[best_i]
        return [root.pose, best.pose]
