from setuptools import setup
#  from torch.utils.cpp_extension import BuildExtension, CUDAExtension, CppExtension
from torch.utils.cpp_extension import BuildExtension, CppExtension
import os
from pathlib import Path
from pybind11.setup_helpers import naive_recompile, ParallelCompile

current_dir = Path(__file__).parent.absolute()

ParallelCompile("NPY_NUM_BUILD_JOBS", needs_recompile=naive_recompile).install()

setup(
    name='pmapcpp',
    ext_modules=[
        CppExtension(
            'pmapcpp',
            sources=[
                'src/pmapcpp.cpp',
                'src/chunkmap.cpp',
                'src/utils.cpp',
                'src/pmap.cpp',
                'src/graph.cpp',
                'src/smp.cpp',
                'src/frontier_map.cpp',
                'src/geom_util.cpp',
            ],
            include_dirs=[os.path.join(current_dir, 'third_party/KDTree/src/'), os.path.join(current_dir, 'include')],
            extra_compile_args={'cxx': ['-g', '-O0'], 'nvcc': ['-O2']},
        )
    ],
    cmdclass={
        'build_ext': BuildExtension
    })
