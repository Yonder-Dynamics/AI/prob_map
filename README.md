# Overall Plan
It's kind of like cleaning
You have some completion percentage and you are trying to perform an action that maximizes the change in completion for the least amount of time.
The only question is, how do we calculate change in completion with some time horizon?
I think we select some time interval and figure out the maximal amount of of change for that amount of time.

# Calculating the reward
If I map the reward onto some space, then warp that space using the terrain difficulty, I can easily calculate the reward of a path
The path traces out a shape on the space, which we can integrate to find the change
The future reward of certain regions are only determined with relation to the large value regions

# Bellman Update?
We can do this using a probabilistic map of where rewards exist.
The problem with trying to apply the Bellman update is that the reward at some state depends on previous states

# MCTS Path search
MCTS to determine the path
There are regions that have some value
Pick regions that you traverse to
Explore regions that have higher entropy

# How to choose regions that you travel to
A region is a contiguous segment of a union of isoclines whose values lie in some range.
You sweep across these segments, which takes some amount of time and gives some reward
I'm calling them isoregions
When the current position and an isoregion is passed in, some output position and time cost are output

# How to run simulation
Install requirements with `pip3 install -r requirements`.
Do `python3 src/pea.py`.
You can view the generated animation in the animation folder.

# Uh oh
The way you generate the path within a region could totally depend on where is valuable to end up
