#pragma once
#include <cstddef>
#include <vector>
#include <string>
#include <torch/extension.h>

#include <limits>
#include <cstdint>
#include <type_traits>

#include "pmap/geom_util.h"

// Code by Wolfgang Brehm on StackOverflow

template<typename T>
T xorshift(const T& n,int i){
  return n^(n>>i);
}

uint32_t distribute(const uint32_t& n);

uint64_t uhash(const uint64_t& n);

// if c++20 rotl is not available:
template <typename T,typename S>
typename std::enable_if<std::is_unsigned<T>::value,T>::type
constexpr rotl(const T n, const S i){
  const T m = (std::numeric_limits<T>::digits-1);
  const T c = i&m;
  return (n<<c)|(n>>((T(0)-c)&m)); // this is usually recognized by the compiler to mean rotation, also c++20 now gives us rotl directly
}

template <class T>
inline std::size_t hash_combine(std::size_t& seed, const T& v)
{
  return rotl(seed,std::numeric_limits<std::size_t>::digits/3) ^ distribute(uhash(v));
}
// END

struct CoordHash
{
    std::size_t operator() (const Coord &pair) const {
        size_t seed = 1337;
        seed = hash_combine(seed, static_cast<uint64_t>(pair.first));
        seed = hash_combine(seed, static_cast<uint64_t>(pair.second));
        return seed;
    }
};

struct uIntHash
{
    std::size_t operator() (const size_t v) const {
        return uhash(static_cast<uint64_t>(v));
    }
};
struct IntHash
{
    std::size_t operator() (const int v) const {
        return uhash(static_cast<uint64_t>(v));
    }
};

std::vector<std::string> split(const std::string& str, const std::string& delim);
torch::Tensor gaussian_kernel(int kernel_size, float sigma);
