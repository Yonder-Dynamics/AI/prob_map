#pragma once
#include "pmap/geom_util.h"
#include <utility>

typedef std::vector<std::vector<int>> ClusterInds;

ClusterInds
nn_cluster(std::vector<Coord> points, std::vector<Coord> centroids)
{
    std::vector<std::vector<int>> clusters (centroids.size());
    for (size_t i=0; i<points.size(); i++) {
        Coord &p = points[i];
        int closest_ind = 0;
        double closest_dist = dist(p, centroids[0]);
        for (size_t j=0; j<centroids.size(); j++) {
            Coord &c = centroids[j];
            double d = dist(p, c);
            if (d < closest_dist) {
                closest_ind = j;
                closest_dist = d;
            }
        }
        clusters[closest_ind].push_back(i);
    }
    return clusters;
}


std::pair<ClusterInds, std::vector<Coord>>
kmeans(int k, std::vector<Coord> points, int iters=3, std::vector<Coord> centroids={})
{
    if (k == 1) {
        Coord center = std::make_pair(0, 0);
        ClusterInds out {{}};
        out[0].reserve(points.size());
        for (size_t i=0; i<points.size(); i++) {
            center.first += points[i].first;
            center.second += points[i].second;
            out[0].push_back(i);
        }
        Coord c = std::make_pair(center.first / (float)points.size() + 0.5, center.second / (float)points.size() + 0.5);
        return std::make_pair(out, std::vector<Coord>({c}));
    }
    // Should I be using floats for coords?
    // Initialize centroids
    std::set<Coord> selected;
    for (int i=centroids.size(); i<k; i++) {
        int ind = rand() % points.size();
        Coord c = points[ind];
        while (selected.find(c) != selected.end()) {
            ind = rand() % points.size();
            c = points[ind];
        }
        centroids.push_back(c);
        selected.emplace(c);
    }
    ClusterInds assignments;
    // Run clustering
    for (int it=0; it<iters; it++) {
        assignments = nn_cluster(points, centroids);
        centroids.clear();
        for (size_t i=0; i<assignments.size(); i++) {
            if (assignments[i].size() == 0) continue;
            Coord c = std::make_pair(0, 0);
            for (size_t j=0; j<assignments[i].size(); j++) {
                Coord &p = points[assignments[i][j]];
                c = c + p;
            }
            c.first = c.first / (float)assignments[i].size() + 0.5;
            c.second = c.second / (float)assignments[i].size() + 0.5;
            centroids.push_back(c);
        }
    }
    return std::make_pair(assignments, centroids);
}
