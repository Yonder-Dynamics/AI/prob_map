#pragma once
#include <torch/extension.h>

#include <vector>
#include <iostream>
#include <fstream>
#include <iterator>
#include <math.h>
#include <algorithm>
#include <utility>
#include <tuple>

#include "pmap/chunkmap.h"
#include "pmap/geom_util.h"
#include "pmap/frontier_map.h"

void init_pmap_module(py::module &m);

class PMap: public FrontierContext {
  float general_path_prob(std::vector<Pose> & path, ChunkMap & tens_map,  bool maxpool);
  void add_gaussian(int x, int y, int std, ChunkMap * map);
  public:
    // Class variables
    int chunk_size;
    float obs_threshold;
    std::string default_prob_gen;
    std::string default_obs_gen;
    ChunkMap obs_map, prob_map, known_map, TP_map, FP_map, TN_map, FN_map, base_tag_prob_map, obs_counter;
    FrontierMap frontier_map;
    float map_res;

    torch::Tensor test(float radius);

    // Functions
    PMap(float obs_threshold=0.1, float map_res=0.1, int chunk_size=100, MapGen default_prob_gen=MapGen::ONES, MapGen default_obs_gen=MapGen::ZEROS);
    torch::Tensor random_pose();
    bool check_path_clear(std::vector<Pose> path);
    void add_qr(int qr_x, int qr_y, int std);
    float dist_to_obstacle(Pose & pose, torch::Tensor ray, float max_d);
    float tpr(float dist);
    float fpr(float dist);
    float tnr(float dist);
    float fnr(float dist);
    void update_tile(int x, int y, float confidence, float dist);
    void add_visited_path(
        std::vector<Pose> path,
        std::vector<torch::Tensor> observations,
        float fx, float cx);
    void add_observation(Pose & pose, torch::Tensor binned_obs,
        float fx, float cx);
    float path_prob(Pose & pose1, Pose & pose2, float max_d,
        float fov);
    void add_obstacle(int obs_x, int obs_y, int std);
    void add_uniform(float x, float y, float radius);
    float obs_prob(std::vector<Pose> & path);

    bool is_free(Coord &c);
    bool is_unknown(Coord &c);

    // float path_prob(std::vector<Pose> & path);
    torch::Tensor get_grid_cells_btw(torch::Tensor p1, torch::Tensor p2);
    std::vector<Node>
    generate_frontier_nodes(int pts_per_cluster);
};
