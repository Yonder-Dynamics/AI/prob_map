#pragma once
#include <torch/extension.h>
#include <unordered_map>
#include "pmap/chunkmap.h"
#include "pmap/geom_util.h"
#include "pmap/graph.h"
#include "pmap/utils.h"

void init_frontier_module(py::module &m);

class FrontierContext {
    public:
        virtual bool is_free(Coord &c) = 0;
        virtual bool is_unknown(Coord &c) = 0;
};

class FrontierMap {
    public:
        ChunkMap frontier_inds;
        std::unordered_map<int, std::list<Coord>, IntHash> frontier_coords;
        int num_frontiers = 0;

        FrontierMap(int chunk_size) {
            this->frontier_inds = ChunkMap(chunk_size, MapGen::NEG_ONES_INT);
        }

        bool
        is_frontier(Coord &c, FrontierContext *ctx);

        void
        delete_frontier(int x, int y);

        void
        add_frontiers(std::vector<Coord> &contour,
                FrontierContext *ctx);

        std::vector<Node>
        generate_clusters(int pts_per_cluster, float remaining_prob,
                std::function< float(Coord&, float) >weight_func);
};
