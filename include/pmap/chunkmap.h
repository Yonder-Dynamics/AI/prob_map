#pragma once
#include "pmap/utils.h"
#include <torch/extension.h>

#include <vector>
#include <iostream>
#include <fstream>

#include <unordered_map>

enum MapGen {
  ZEROS = 0,
  ONES = 1,
  NEG_ONES_INT = 2,
};

const float CMAP_NIL = -1e10;

void init_chunkmap_module(py::module &m);

struct Chunk {
  torch::Tensor data;
  bool tag;
};

struct ChunkMap {
  public:
    // Class variables
    int chunk_size;
    MapGen default_gen;
    int cxmin, cymin, cxmax, cymax;
    std::unordered_map<Coord, Chunk, CoordHash> chunks;

    // Functions
    ChunkMap(int chunk_size=100, MapGen default_gen=MapGen::ZEROS);
    Chunk gen_empty();

    torch::Tensor default_value();
    bool contains_chunk(int cx, int cy);
    std::tuple<torch::Tensor, torch::Tensor> get_bounds();
    torch::Tensor random_pose();
    torch::Tensor sum();

    torch::Tensor chunk_at(int cx, int cy);
    void chunk_set(int cx, int cy, Chunk chunk);

    torch::Tensor at(int x, int y);
    void set(int x, int y, torch::Tensor value);
    void operator*=(torch::Tensor other);

    std::tuple<torch::Tensor, torch::Tensor>
    stitch_map(c10::optional<int> lcxmin=c10::nullopt,
            c10::optional<int> lcxmax=c10::nullopt,
            c10::optional<int> lcymin=c10::nullopt,
            c10::optional<int> lcymax=c10::nullopt);
    void add_grid(int x, int y, torch::Tensor grid);
};

