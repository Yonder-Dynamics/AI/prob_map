#include "octree/cuda_raycaster.h"

using namespace octree;

namespace cudatree {

/* typedef float3 float3; */

typedef struct FLinkedList {
  float score;
  int address;
  int child;
} FLinkedList;

// Here is how the voxels are numbered
__device__ float3 VOXEL_NUMBERING[] = {
  { 1,  1,  1},
  { 1,  1, -1},
  { 1, -1,  1},
  { 1, -1, -1},
  {-1,  1,  1},
  {-1,  1, -1},
  {-1, -1,  1},
  {-1, -1, -1}
};
__device__ int64_t NULL_VOX = -1;
__device__ float EPS = 1e-6;
__device__ const size_t STACK_SIZE = 4000;
/* __constant float EPS = 0; */

__device__
int
sort_insert(
    FLinkedList * list,
    const int head_index,
    const int desired_address,
    const float score)
{
  // Returns the new head index
  int new_head_index = head_index;
  int selection = head_index;
  int parent = head_index;
  int child = -1;
  // Traverse linked list and sort
  if (head_index == -1) {
    // init linked list
    list[0].address = desired_address;
    list[0].child = child;
    list[0].score = score;
    return 0;
  } else {
    while (selection != -1 && list[selection].score < score) {
      parent = selection;
      selection = list[selection].child;
    }
    if (selection == parent) {
      if (list[selection].score > score) {
        child = head_index;
        new_head_index = desired_address;
      } else {
        list[parent].child = desired_address;
      }
    } else {
      child = selection;
      list[parent].child = desired_address;
    }
  }
  list[desired_address].address = desired_address;
  list[desired_address].child = child;
  list[desired_address].score = score;
  return new_head_index;
}

__device__
bool
rayBoxIntersect ( float3 rpos, float3 rdir, float3 vmin, float3 vmax, float * ray_length)
{
  // posted by zacharmarz
  // https://gamedev.stackexchange.com/questions/18436/most-efficient-aabb-vs-ray-collision-algorithms

  // rdir is unit direction vector of ray
  float3 dirfrac = {1/rdir.x, 1/rdir.y, 1/rdir.z};
  // rpos is ORIGIN of ray
  float t1 = (vmin.x - rpos.x)*dirfrac.x;
  float t2 = (vmax.x - rpos.x)*dirfrac.x;
  float t3 = (vmin.y - rpos.y)*dirfrac.y;
  float t4 = (vmax.y - rpos.y)*dirfrac.y;
  float t5 = (vmin.z - rpos.z)*dirfrac.z;
  float t6 = (vmax.z - rpos.z)*dirfrac.z;

  float tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
  float tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));

  // if tmax < 0, ray (line) is intersecting AABB, but the whole AABB is behind us
  if (tmax < 0)
  {
      *ray_length = tmax;
      return false;
  }

  // if tmin > tmax, ray doesn't intersect AABB
  if (tmin > tmax)
  {
      *ray_length = tmax;
      return false;
  }

  *ray_length = tmin;
  return true;
}

__device__
int64_t getNodeChild(int64_t * data, int64_t i, int64_t j) {
  return data[i*8+j];
}

__device__
int64_t
raycast(OctreeConfig* tree_cfg,
        const float3 pos,
        const float3 dir,
        float * dist,
        int64_t * data)
{
  // The first thing we need to do is establish a traversal order for the voxels
  // Sort voxels by distance in increasing order

  // Initialize list with ORIGIN and depth 0 key
  // keys are terminated with -1

  // We have a rotating stack
  // Take the head
  // Iterate through the 8 children and sort them by distance
  // put new octrees on top of stack in order of distance
  // loop
  float length = sqrt(dir.x*dir.x+dir.y*dir.y+dir.z*dir.z);
  if (length == 0) {
    return NULL_VOX;
  }
  float3 ray = {
    dir.x/length,
    dir.y/length,
    dir.z/length,
  };

  // initialize stack
  float3 voxel_centers[STACK_SIZE];
  int depths[STACK_SIZE];
  // Initialize the depths so we know when we run out
  for (int i=0; i<STACK_SIZE; i++) {
    depths[i] = -1;
  }
  int64_t voxel_inds[STACK_SIZE];

  int stack_head = 0;
  voxel_centers[stack_head] = tree_cfg->origin;
  depths[stack_head] = 0;
  voxel_inds[stack_head] = 0;
  int stop_depth = tree_cfg->max_depth;

  while (depths[stack_head] != -1) {
    float3 voxel_origin = voxel_centers[stack_head];
    int64_t voxel_ind = voxel_inds[stack_head];
    int depth = depths[stack_head];
    float mul = (depth == stop_depth) ? 1 : -1;

    int head = -1;
    float s = tree_cfg->res*pow(2.0f, tree_cfg->max_depth-depth);
    FLinkedList list[8];
    // for (int i=0; i<8; i++) {
    //   list[i].child = -1;
    //   list[i].score = 999999;
    //   list[i].address = -1;
    // }
    float3 centers[8];
    for (int i=0; i<8; i++) {
      if (getNodeChild(data, voxel_ind, i) == NULL_VOX) {
        continue;
      }
      float3 bound = VOXEL_NUMBERING[i];

      /* centers[i] = voxel_origin + s*bound/2.0f; */
      float3 center = {
        voxel_origin.x + s*bound.x/2.0f,
        voxel_origin.y + s*bound.y/2.0f,
        voxel_origin.z + s*bound.z/2.0f,
      };
      centers[i] = center;
      /* float3 vmin = centers[i] - s/2.0f-EPS; */
      float3 vmin = {
        centers[i].x - s/2.0f-EPS,
        centers[i].y - s/2.0f-EPS,
        centers[i].z - s/2.0f-EPS,
      };
      /* float3 vmax = centers[i] + s/2.0f+EPS; */
      float3 vmax = {
        centers[i].x + s/2.0f+EPS,
        centers[i].y + s/2.0f+EPS,
        centers[i].z + s/2.0f+EPS,
      };

      float ray_length;
      bool collision = rayBoxIntersect(pos, ray, vmin, vmax, &ray_length);

      if (!collision) {
        continue;
      }
      // sort backwards so that when we append them on to the top of the stack they are in the right order
      head = sort_insert(list, head, i, mul*ray_length);
    }
    int selection = head;

    // Iterate through the sorted voxels
    while (selection != -1) {
      // Update index
      int j = list[selection].address;
      float d = mul*list[selection].score;
      selection = list[selection].child;
      int64_t new_ind = getNodeChild(data, voxel_ind, j);

      // Check if voxel is empty
      if (new_ind == NULL_VOX) {
        continue;
      }

      // If this is the maximum depth, return
      if (depth == stop_depth) {
        *dist = d;
        return voxel_ind*8+j;
      }
      // Push onto the top of the stack
      // set stack at head to have the new variables:
      voxel_centers[stack_head] = centers[j];
      voxel_inds[stack_head] = new_ind;
      depths[stack_head] = depth+1;
      stack_head = (stack_head-1 + STACK_SIZE) % STACK_SIZE;
    }
    stack_head = (stack_head+1)%STACK_SIZE;
  }
  *dist = 0;
  return NULL_VOX;
}


// Helper functions for projection

__device__
float3
rotateVec(float * mat, float3 v) {
  float3 out;
  out.x = mat[0] * v.x + mat[1] * v.y + mat[2] * v.z;
  out.y = mat[4] * v.x + mat[5] * v.y + mat[6] * v.z;
  out.z = mat[8] * v.x + mat[9] * v.y + mat[10] * v.z;
  return out;
}

__device__ void
calcRayPos(CameraConfig * cfg, float i, float j, float3 * pos, float3 * ray) {
    float3 forward_ray = {
      (j - cfg->cx) / cfg->fx,
      (i - cfg->cy) / cfg->fy,
      1.0f};
    *ray = rotateVec(cfg->transform_matrix, forward_ray);
    *pos = {cfg->transform_matrix[3], cfg->transform_matrix[7], cfg->transform_matrix[11]};
}

};
