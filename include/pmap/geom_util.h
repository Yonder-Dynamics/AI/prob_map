#pragma once
#include <torch/extension.h>
#include <utility>
#include "Point.h"

void init_geom_module(py::module &);
typedef std::pair<int, int> Coord;

struct AngRay {
    float angle, conf, dist, euclidean_dist;
    Coord ray_tip(int ox=0, int oy=0, float ang_offset=0, float offset=0) {
        return Coord(cos(angle+ang_offset)*(euclidean_dist+offset)+ox, sin(angle+ang_offset)*(euclidean_dist+offset)+oy);
    }
    bool operator<(const AngRay& other) const {
        return angle < other.angle;
    }
};

struct Ray{
    float origin_dist; 
    int x, y;
    bool operator<(const Ray& other) const {
        return origin_dist < other.origin_dist;
    }
};

struct Pose {
  public:
    float x, y, theta;
    Pose(torch::Tensor position, torch::Tensor orientation):
      x(position[0].item<float>()), y(position[1].item<float>()), theta(orientation.item<float>()) {}
    Pose(float x, float y, float theta) : x(x), y(y), theta(theta) { }
    torch::Tensor position() {
      return torch::tensor({x, y});
    }

    Coord coord() {
      return std::make_pair(x, y);
    }

    Pose clone() {
      return Pose(x, y, theta);
    }

    float cost(Pose &p2) {
      return sqrt(x*x + y*y);
    }

    float distance(Pose &p2) {
      return sqrt((x-p2.x)*(x-p2.x) + (y-p2.y)*(y-p2.y));
      // return torch::norm(this->position-p2.position).item<float>();
    }
    Point<2> to_point() {
      Point<2> p;
      p[0] = x;
      p[1] = y;
      return p;
    }
};

Point<2> pose_to_point(Pose pose);


torch::Tensor rot(float angi);
torch::Tensor rotX(float angi);
torch::Tensor rotY(float angi);
torch::Tensor rotZ(float angi);


template <typename T,typename U>                                                   
std::pair<T,U> operator+(const std::pair<T,U> & l,const std::pair<T,U> & r) {   
    return {l.first+r.first,l.second+r.second};                                    
}

void create_line( std::vector<Coord> &out, float x1, float y1, float x2, float y2);
float dist(Coord &a, Coord &b);

const std::vector<std::pair<int, int>> NEIGHBORS {
  {+1, 0}, {1, 1}, {1, -1},
  {0, -1}, {0, +1},
  {-1, 0}, {-1, 1}, {-1, -1}
};

std::vector<AngRay>
obs_to_rays(Pose & pose, torch::Tensor &binned_obs, float fx, 
    float cx, float map_res);

std::vector<Coord>
rays_to_contour(std::vector<AngRay> &rays, Pose &pose);