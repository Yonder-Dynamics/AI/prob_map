#pragma once
#include <torch/extension.h>

#include <unordered_map>
#include <utility>
#include <vector>
#include <iostream>
#include <fstream>
#include "KDTree.h"
#include "Point.h"
// #include "utils.h"
#include "pmap/utils.h"
#include "pmap/geom_util.h"

const float inf = std::numeric_limits<float>::infinity();
void init_graph_module(py::module &);

// TODO: add length method

struct Edge {
  public:
    int from_i, to_i;
    float base_value, future_value, length;
    std::vector<Pose> path;
    Edge(int from_i, int to_i, std::vector<Pose> path, float base_value) :
      from_i(from_i), to_i(to_i), base_value(base_value),
      future_value(0), path(path) {
        this->length = 0;
        for (size_t i = 1; i < path.size(); i++){
          this->length += path.at(i-1).distance(path.at(i));
        }
    }
    void add_pose(Pose p) {
      path.push_back(p);
    }
};

struct Node {
  public:
    float cost, base_value;
    bool is_frontier;
    int index;
    Pose pose;
    Node *parent;
    std::list<Node *> children;
    Node():
      cost(0), base_value(0), is_frontier(false), index(-1), pose(0, 0, 0), parent(NULL) {}
    Node(Pose pose, Node *parent):
      cost(0), base_value(0), is_frontier(false), index(-1), pose(pose), parent(parent) {}

    int getDepth() {
      int depth = 0;
      Node * curr = parent;
      while (curr != NULL) {
        curr = curr->parent;
        depth++;
      }
      return depth;
    }

    float recursive_value(float gamma) {
      // Adds up the cost to the root node
      if (children.size() == 0) {
        return base_value;
      }
      float weight = ((parent == NULL) ? 0 : pose.distance(parent->pose)) + 1;
      std::vector<float> returns {base_value/weight};
      for (auto child : children) {
        float val = gamma * child->recursive_value(gamma);
        returns.push_back(val);
        // if (parent == child) {
        //   printf("Incest\n");
        //   printf("%p, %p, %p, %p\n", child, child->parent, this, parent);
        // }
      }
      float rval = *std::max_element(returns.begin(), returns.end());
      // for (float ele : returns) {
      //   printf("rval: %f, %f\n", rval, ele);
      // }
      return rval;
    }

    float recursive_cost()
    {
      // Adds up the cost to the root node
      Node* curr = parent;
      if (curr == NULL) return 0;
      float rcost = pose.distance(parent->pose);
      int i = 0;
      while (curr->parent != NULL)
      {
        if (curr->parent->cost == inf)
        {
          return inf;
        }
        // printf("Recurse: %i. Curr: %p. Cost: %f, %f\n", i, curr, rcost, curr->pose.distance(curr->parent->pose));
        rcost += curr->pose.distance(curr->parent->pose);
        curr = curr->parent;
        i += 1;
      }
      return rcost;
    }
};

// replacement for KDTree to find nearest neighbors dynamically
struct GridMap
{
  // typedef std::string IND_TYPE;
public:
  // map from string rep of cunk to a list of nodes
  std::unordered_map<Coord, std::list<int>, CoordHash> gridmap;
  int chunk_size;
  unsigned int n_index;
  GridMap(int size) : chunk_size(size), n_index(0) {}

  void insert(Point<2> point)
  {
    int cx = floor(point[0] / chunk_size);
    int cy = floor(point[1] / chunk_size);
    Coord cind = std::make_pair(cx, cy);
    if (gridmap.find(cind) == gridmap.end())
    {
      gridmap[cind] = {};
    }
    gridmap[cind].push_back(n_index);
    n_index++;
  }

  bool empty()
  {
    return gridmap.empty();
  }

  std::list<unsigned int> getNearestNeighbors(Point<2> point, int r)
  {
    //printf("Got into getNearestNeighbors: point- (%f, %f)\n", point[0], point[1]);
    std::list<unsigned int> near_neighbors;
    int cx = floor(point[0] / chunk_size);
    int cy = floor(point[1] / chunk_size);

    for (int ci = cx - r; ci <= cx + r; ci++)
    {
      for (int cj = cy - r; cj <= cy + r; cj++)
      {
        Coord neighbor = std::make_pair(ci, cj);
        if (gridmap.find(neighbor) == gridmap.end())
        {
          continue;
        }
        near_neighbors.insert(near_neighbors.end(), gridmap[neighbor].begin(), gridmap[neighbor].end());
      }
    }
    return near_neighbors;
  }
};

struct Graph {
  public:
    size_t root_ind;
    std::unordered_map<size_t, Node, uIntHash> nodes;
    size_t last_open_ind = 0;
    GridMap gmap;
    Graph(Node n, int chunk_size) : root_ind(0), gmap(chunk_size) {
      add_node(n);
      printf("Chunksize: %f\n", chunk_size);
    }
    // void add_edge(Edge e) {
    //   edges.push_back(e);
    // }
    size_t add_node(Node n) {
      // Returns index node was inserted at
      gmap.insert(n.pose.to_point());
      n.index = last_open_ind;
      nodes[last_open_ind] = n;
      // nodes.push_back(n);
      return last_open_ind++;
    }

    void erase_node(size_t ind) {
      // remove from gmap
      Node &n = nodes.at(ind);
      Coord c = std::make_pair((int)n.pose.x, (int)n.pose.y);
      gmap.gridmap.at(c).remove(ind);
      for (auto child : n.children) {
        erase_node(child->index);
      }

      // Erases node at index ind
      // nodes.erase(nodes.begin() + ind);
      nodes.erase(ind);
    }

    size_t size() {
      return nodes.size();
    }

    Node * get_node(size_t ind) {
      return &nodes.at(ind);
    }

    Node * root() {
      return get_node(root_ind);
    }

    void change_root(size_t ind) {
      Node *new_root = get_node(ind);
      Node *old_root = get_node(root_ind);

      new_root->parent->children.remove(new_root);
      new_root->parent = NULL;
      new_root->children.push_back(old_root);
      old_root->parent = new_root;

      root_ind = ind;
    }

    std::pair<unsigned int, float> get_closest_node(Pose pose) {
      return get_closest_nodes(pose)[0];
    }

    std::vector<std::pair<unsigned int, float>> get_neighbors(Pose pose, float radius) {
      std::vector<std::pair<unsigned int, float>> out;
      Point<2> p = pose.to_point();
      std::list<unsigned int> neighborNodes = gmap.getNearestNeighbors(p, ceil(radius/gmap.chunk_size));
      for (int index : neighborNodes) {
        Node *it = get_node(index);

        float d = Distance(p, it->pose.to_point());
        if (d > radius) continue;
        auto ele = std::make_pair<unsigned int, float>( (unsigned int)index, (float)d );
        out.push_back(ele);
      }
      return out;
    }

    std::vector<std::pair<unsigned int, float>> get_closest_nodes(Pose pose, int k=1) {
      std::vector<std::pair<unsigned int, float>> out;
      Point<2> p = pose.to_point();
      int r = 1;
      std::list<unsigned int> neighborNodes = gmap.getNearestNeighbors(p, r);;
      // BoundedPQueue<unsigned int> pQueue(k);
      if (gmap.empty()) return out;

      while ((int) neighborNodes.size() < std::min(k, (int) nodes.size()))
      {
        //printf("neighborNodes: %f\n", neighborNodes.size());
        r++;
        neighborNodes = gmap.getNearestNeighbors(p, r);
      }

      for (int index : neighborNodes) {
        auto ele = std::make_pair<unsigned int, float>(
          std::forward<unsigned int>(index),
          Distance(p, get_node(index)->pose.to_point())
        );
        out.push_back(ele);
      }
      auto cmp_dist = [](std::pair<unsigned int, float>& p1, std::pair<unsigned int, float>& p2) -> bool {
        return p1.second < p2.second;
      };

      if ((int) out.size() > k) {
        std::nth_element(out.begin(), out.begin() + k, out.end(), cmp_dist);
      }

      std::vector<std::pair<unsigned int, float>> k_out (out.begin(), out.begin()+k);
      return k_out;
    }

    void print() {
      printf("Root ind: %zu\n", root_ind);
      for (auto it : nodes) {
        printf("Node: %zu. Address: %p. Parent: %p. Children: ", it.first, &(it.second), it.second.parent);
        for (auto c : it.second.children) {
          printf("%p, ", c);
        }
        printf("\n");
      }
    }

};

