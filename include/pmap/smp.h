#include "pmap/graph.h"
#include "pmap/pmap.h"
#include <unordered_map>

using namespace std::chrono;

void init_smp_module(py::module &m);
struct node_compare {
	bool operator() (const Node* lhs, const Node* rhs) const {
		if ((lhs->pose.x != rhs->pose.x))
			return (lhs->pose.x < rhs->pose.x);
		else
			return (lhs->pose.y < rhs->pose.y);
	}
};

class RTRRTstar {

  public:
    bool goal_found = false;
    bool sampled_in_goal_region = false;
    bool using_informed_rRTstar = false;
    bool goal_defined = false;
    system_clock::time_point last_recorded_time; // TODO Initialize
    float allowed_time_rewiring, rrtstarradius, epsilon, sample_radius;
    float max_d, fov;

	Graph graph;
    std::vector<Node> queued_goals;
    std::vector<Node*> added_goals;
    PMap *map;
    Node *target = NULL;
    Node *next_target = NULL;
    std::set<Node*, node_compare> visited_set;
	std::list<Node*> curr_path, rewire_rand, rewire_root, already_rewired;

	float elapsed_time();
	bool is_sample_clear(Pose &p);
	bool is_path_clear(Pose &p1, Pose &p2);
	size_t add_node(Pose p, Node* closest,
			std::vector<std::pair<unsigned int, float>> neighbors);
	void next_iter(Pose & robot_pose);
	void change_root(size_t node_ind);
	bool expand_and_rewire(Pose &pose);
	// void update_next_best_path();
	Pose uniform_sample();
	Pose sample();

	void update_frontier();
	void rewire_random_node();
	void rewire_from_root();
	float get_heuristic(Node* u);
    void paint_graph();
	bool try_connect_goal(size_t node_ind);

	RTRRTstar(Pose initial_pos, PMap &map, int chunk_size, float allowed_time_rewiring, float rrtstarradius, float epsilon,
			  float sample_radius, float max_d, float fov) :
      allowed_time_rewiring(allowed_time_rewiring), rrtstarradius(rrtstarradius), epsilon(epsilon), sample_radius(sample_radius), max_d(max_d), fov(fov),
      graph(Node(initial_pos.clone(), NULL), chunk_size), map(&map)
	{
		last_recorded_time = system_clock::now();
		printf("Chunk size: %i\n", chunk_size);
		duration<double> ms = system_clock::now() - last_recorded_time;
		printf("%f\n", ms.count());
		srand(ms.count());
	}
};

