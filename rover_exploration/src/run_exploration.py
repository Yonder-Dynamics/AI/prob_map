#!/usr/bin/env python3
import rrt
import rospy
import time
from darknet_ros_msgs.msg import BoundingBox, BoundingBoxes, CheckForObjectsAction, CheckForObjectsGoal
from geometry_msgs.msg import Twist, Vector3
from sensor_msgs.msg import Image
from pmapcpp import Node, Graph, Edge, Pose, PMap
import torch
from cv_bridge import CvBridge, CvBridgeError
from pathlib import Path
import math
from matplotlib import colors
from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np

gamma = 0.99
bridge = CvBridge()

def assign_edge_values(graph, pmap):
    paths = [e.path for e in graph.edges]
    start_time = time.time()
    vals = pmap.path_probs(paths, 5)
    print(time.time() - start_time)
    for e, v in zip(graph.edges, vals):
        e.base_value = v

class ImageBuf:
    """
    The depth image should arrive before the bounding boxes, since bounding boxes go through yolo first.
    Because of that, we can buffer the depth images.
    """
    def __init__(self):
        self.buf = []

    def received(self, message):
        self.buf.append((message.header.stamp, bridge.imgmsg_to_cv2(message, "32FC1")))

    def get(self, stamp):
        """
        Searches buffer for corresponding depth map to find a depth map with the correct timestamp,
        flushing buffer during search
        """
        while len(self.buf) != 0:
            cur_stamp, depth_img = self.buf.pop(0)
            if cur_stamp == stamp:
                return depth_img
        return None

class ExplorationController:
    def __init__(self):
        self.buf = ImageBuf()
        self.bbox_sub = rospy.Subscriber('/armodel/bboxes', BoundingBoxes, self.obs_received)
        self.depth_img_sub = rospy.Subscriber('/zed/zed_node/depth/depth_registered', Image, self.buf.received)
        self.waypoint_pub = rospy.Publisher('/rover_waypoint', Twist)

        Path("animation").mkdir(exist_ok=True)
        # Generate pmap
        self.root_pos = Pose(torch.tensor([0, 0]), torch.tensor([0]))

        self.print_graph = True
        self.print_path_step = False

        # Generate pmap
        self.pmap = PMap(res=1, chunk_size=50)

        #  num_tags = 50
        (prob_min_x, prob_min_y), (prob_max_x, prob_max_y) = pmap.prob_map.get_bounds()
        (obs_min_x, obs_min_y), (obs_max_x, obs_max_y) = pmap.obs_map.get_bounds()

        # absolute extreme coordinates
        self.min_x = min(prob_min_x, obs_min_x)
        self.min_y = min(prob_min_y, obs_min_y)
        self.max_x = max(prob_max_x, obs_max_x)
        self.max_y = max(prob_max_y, obs_max_y)

        # convert to chunk coordinates
        self.min_cx = min_x // pmap.prob_map.chunk_size
        self.min_cy = min_y // pmap.prob_map.chunk_size
        self.max_cx = max_x // pmap.prob_map.chunk_size
        self.max_cy = max_y // pmap.prob_map.chunk_size

        # COLORS
        self.norm = colors.LogNorm(vmin=0.00000001, vmax=1)

        self.N = 400
        self.main_path = [self.root_pos]
        self.dist_per_iter = 5.0
        self.binned_obs_n = 21
        self.max_dist = 50
        self.negative_obs = 0.05

    def __del__(self):
        self.bbox_sub.unregister()
        self.depth_img_sub.unregister()

    def bounding_box_to_binned_obs(self, bounding_boxes, depth_img):
        #TODO: Do maybe do this non-naively, handle edges better?
        obs = torch.ones((self.binned_obs_n, 2))
        obs[:, 0] *= self.negative_obs
        obs[:, 1] *= self.max_dist

        for box in bounding_boxes:
            lower = self.binned_obs_n * (box.xmin / depth_img.shape[1])
            upper = self.binned_obs_n * (box.max / depth_img.shape[1]) + 1
            for i in range(lower, upper):
                if box.probability < obs[i, 0]:
                    obs[i, 0] = box.probability
                    #TODO: maybe make it so that the distance is the median among the slice of the image
                    # rather than the whole observation. I think in practice it shouldn't make a difference
                    # since the bounding box's depth should be the same. Also, here I do median over mean
                    # since I think that will be more accurate to the distance of the box, though this isn't tested.
                    obs[i, 1] = np.median(depth_img[box.ymin:box.ymax, box.xmin:box.xmax])
        return obs

    def obs_received(self, message):
        depth_img = self.buf.get(message.header.stamp)
        bounding_boxes = message.bounding_boxes
        if depth_img != None:
            print(depth_img.shape)
            graph = Graph()
            root = Node(0, self.root_pos, 0, -1)
            graph.add_node(root)

            rng = torch.linspace(self.dist_per_iter, 80, N)
            start_time = time.time()
            print("Running RRT*")
            start_time = time.time()
            obs_cache_map, origin = self.pmap.obs_map.stitch_map()
            # First, add a spoke pattern
            rrt.add_spokes(graph, self.pmap, obs_cache_map, origin, self.dist_per_iter)
            n = 0
            no_add = 0
            i = 0
            while i < self.N and no_add < 10:
                res = rrt.iterate_rrt(graph, self.pmap, obs_cache_map, origin, rng[i], 5)
                if res == 0:
                    i += 1
                    no_add = 0
                if res == -1:
                    no_add += 1
                if res != -2:
                    n += 1
            print(f"Efficiency: {i/n} nodes retained")
            print(f"Took {time.time()-start_time}")

            print("Assigning")
            assign_edge_values(graph, self.pmap)
            graph.update_future_values(0, gamma)

            # Find best path towards tags
            traved_path, edge_index = graph.extract_best_edge(0)
            #  traved_path = path
            #  main_path += [path[-1]]
            main_path += traved_path

            self.root_pos = traved_path[-1]

            waypoint_msg = Twist()
            waypoint_msg.linear = Vector3(x=self.root_pos.position[0], y=self.root_pos.position[1])
            waypoint_msg.angular = Vector3(z=self.root_pos.orientation[0])
            self.waypoint_pub.publish(waypoint_msg)

            observ = bounding_box_to_binned_obs(bounding_boxes, depth_img)

            # Gather nodes to plot
            disp, origin = self.pmap.prob_map.stitch_map(self.min_cx, self.max_cx, self.min_cy, self.max_cy)
            obs_disp, _ = self.pmap.obs_map.stitch_map(self.min_cx, self.max_cx, self.min_cy, self.max_cy)

            if self.print_graph:
                plt.clf()
                #  disp_obs(root_pos, observ)
                plt.imshow(disp.T, extent=(self.min_x, self.max_x, self.min_y, self.max_y), origin='lower', cmap='hot', norm=self.norm)
                plt.imshow(obs_disp.T, extent=(self.min_x, self.max_x, self.min_y, self.max_y), origin='lower', alpha=0.3, cmap='Blues')
                plt.colorbar()
                max_weight = max(e.base_value for e in graph.edges)
                max_weight = 1 if max_weight == 0 else max_weight
                for e in graph.edges:
                    p1 = graph.nodes[e.from_i].pose.position.numpy()
                    p2 = graph.nodes[e.to_i].pose.position.numpy()
                    plt.plot([p1[0], p2[0]], [p1[1], p2[1]], c=cm.hot(float(e.base_value / max_weight)))
                x, y = zip(*[pose.position for pose in main_path])
                plt.plot(x, y, c="black", linewidth=1.1)
                plt.plot(x, y, c="green", linewidth=1.0)

                plt.show()
            if graph.end_condition(edge_index, 0.7):
                # TODO send waypoints that circle around tag
                pass
        else:
            print("O NO! NO DEPTH IMG FOUND?! U DUM?")

if __name__ == "__main__":
    ExplorationController()
